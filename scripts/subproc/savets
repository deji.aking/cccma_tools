#! /bin/sh
 
#    Dec 18/92 - F.Majaess
 
#id  savets - Accumulates time series data.
 
#    AUTHOR  - F.Majaess

#hd  PURPOSE - "savets" script is used to accumulate new time series
#hd            data in "ts" file onto an existing time series file
#hd            "tsfile".
 
#pr  PARAMETERS:
#pr
#pr    POSITIONAL 
#pr
#pr      ts     = name of new time series data file.
#pr      tsfile = name of time series file to which "ts" data
#pr               is appended to.
 
#ex  EXAMPLE: 
#ex
#ex          savets newts tsfile 
#ex
 
IFS=' '
export IFS
 
#   * code used to set switches and deal with parameter=value arguments 
#   * as well as setting parameters to their primary/secondary defaults.
#   * The list of other arguments (if any) is returned in "prmtrl" var...
 
#  *  check if switches/parameters specified on script call
 
if [ $# -gt 0 ] ; then
  
#  * put argument list in "prmtrl" ...
   
  prmtrl=$@
  prmtrl=`echo $prmtrl | tr '[A-Z]' '[a-z]'`
  
#  * for each argument specified do...
    
  for prmtr in $prmtrl
  do
    case $prmtr in
 
#   * check for standard shell switches and set them accordingly
 
     --|-|[-+][aefhkntuvx]) set `echo $prmtr ` ;;
 
#   * case where a parameter is to be assigned a value
 
    *=*   ) eval $prmtr ;;
 
#   * anything else case skip...
 
    *  )  tprmtr="$tprmtr $prmtr";;
 
    esac
  done
  prmtrl=$tprmtr
  tprmtr=
 
fi

#   * make sure arguments list variable "prmtrl" is initialized...
 
prmtrl=${prmtrl=}
  
#   * The following code deals with checking/setting positional 
#   * parameters ...
 
if [ -n "$prmtrl" ] ; then
  i=1
  for prmtr in $prmtrl
  do
    case $i in
      1) ts=$prmtr;;
      2) tsfile=$prmtr;;
      *) eval "echo '$0 : Skipped $prmtr !' " ;;
    esac
    i=`expr $i + 1`
  done
fi
 
#   * Prompt for a positional parameter value(s) if none was/were
#   * specified...
 
while [ -z "$ts" ] ; do
  echo "please enter new time series data filename ? > \\c"
  read ts
done
 
while [ -z "$tsfile" ] ; do
  echo "please enter existing time series data filename ? > \\c"
  read tsfile
done
 
#   * Task of the script...
# set -x

access oldts $tsfile na &&
joinup newts oldts $ts &&
save newts $tsfile &&
delete oldts na &&
release oldts newts ||
exit 1

