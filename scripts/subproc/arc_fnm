#! /bin/sh

#    May 31/2004 - F.Majaess

#id arc_fnm - "arcfile" filename mapping from individual filenames list.

#   AUTHOR  - F.Majaess

#hd PURPOSE - "arc_fnm" code is used to map to a suitable 
#hd           "arcfile" filename (returned in $arcfile) based on 
#hd           the individual files list passed on via "Tlist" 
#hd           variable.
#hd           "arclabel" setting is used in constructing "arcfile" filename  
#hd           if it is specified.
#hd           Note: The script is not intended to be called directly by
#hd                 the user, instead it's to be called by higher level
#hd                 scripts.

# Abort if the list of individual files is not specified
# and "arcfile" variable is not set...

if [ -z "$arclabel" -a -z "$Tlist" ] ; then
 echo "arc_fnm: Sorry, the list of individual files must be supplied via Tlist parameter!"
 if [ -n "$Prevwd" -a -n "$Tmpcmcarcdir" ] ; then
  # If applicable, cleanup temporary subdirectory and switch back
  # to previous subdirectory's path ... 
  cd $Prevwd
  rm -r -f $Tmpcmcarcdir
 fi
 exit 1
fi


#   * If necessary, set in "offcldir" parameter the official 
#   * subdirectory's path on the archive server...
#   * And other parameters...   

user=${user:=$USER}
if [ "$SITE_ID" = 'DrvlSC' ] ; then
 offcldir=${offcldir:-'/archive/eccc/crd/ccrn/offcl_data'}
 LgnameEC:${LgnameEC:-`accmap`}
else
 offcldir=${offcldir:-'/home/cfs_ccrd/ccrn/offcl_data'}
 if [ -n "$user" ] ; then
  Lgname=${Lgname:-`echo $user | tr '[A-Z]' '[a-z]'`}
 else
  Lgname=${Lgname:-`whoami`}
 fi
 LgnameEC="$Lgname"
fi

if [ -n "$arclabel" ] ; then
 arcfile="$arclabel"
else
 unset arcfile
fi

#   * Note: The code below may reference the following ...
#   *  Tmpcmcarcdir - Usually path to current working directory
#   *  Prevwd - Path to revious working directory (usually ../)
#   *  savdir - user target subdirectory's path
#   *  usr - User account's identifier (username's last 3 characters) 

usr=${usr:-`echo $LgnameEC | sed -e 's/^....//'`}

if [ -z "$arcfile" -a -n "$Tlist" ] ; then
 # set -x
 for rname in $Tlist
 do

  # Abort if any of the filename terminates with "_arc"

  Arc_extnsn=`echo $rname | tr '[A-Z]' '[a-z]' | sed -n -e 's/^.*_arc$/Yes/p'`
  if [ "$Arc_extnsn" = 'Yes' ] ; then
   echo "arc_fnm: Illegal _arc extension detected in $rname!"
   if [ -n "$Prevwd" -a -n "$Tmpcmcarcdir" ] ; then
    # If applicable, cleanup temporary subdirectory and switch back
    # to previous subdirectory's path ... 
    cd $Prevwd
    rm -r -f $Tmpcmcarcdir
   fi
   exit 1
  fi

 done

 # set +x

 arcfile=`arc_shrd $Tlist`

 # echo "arcfile=$arcfile"
 
fi

# echo "arcfile=$arcfile"

Date=${Date:-`date -u '+%Y%j'`}
Time=${Time:-`date -u '+%H%M%S'`}

if [ -z "$arcfile" ] ; then
 # echo "arc_fnm: No mapped arcfile filename in arcfile parameter!"
 # if [ -n "$Prevwd" -a -n "$Tmpcmcarcdir" ] ; then
 #  # If applicable, cleanup temporary subdirectory and switch back
 #  # to previous subdirectory's path ... 
 #  cd $Prevwd
 #  rm -r -f $Tmpcmcarcdir
 # fi
 # exit 1
 unset Arc_extnsn
 arcfile="${Date}${Time}_arc"
else

 arcfile=`echo $arcfile | tr '[A-Z]' '[a-z]'`

 # Add "Date+Time_arc" suffix (if necessary) ...

 Arc_extnsn=`echo $arcfile | sed -n -e 's/^.*_arc$/Yes/p'`
 if [ "$Arc_extnsn" != 'Yes' ] ; then
  arcfile="$arcfile""_${Date}${Time}_arc"
 fi
fi

# echo "arcfile=$arcfile"

# If so, set "arcfile_list" appropriately for the filename 
# use in maintaining the list of individual files added to 
# "$arcfile" ...
  
if [ -n "$offcldir" -a "$savdir" != "$offcldir" ] ; then

   # Add "u${usr}_" prefix (if need be) in the case of the archival 
   # is targeting other than the official data subdirectory ...

   Usrprfx=`echo $arcfile | $AWK -F '_' '{print $1}'`
   if [ "$Usrprfx" != "u${usr}" ] ; then
    arcfile="u${usr}_${arcfile}"
    # echo "User prefix added, arcfile=$arcfile"
   fi
   arcfile_list=".${LgnameEC}_${arcfile}"

   Arcnfld=`echo $arcfile | $AWK -F '_' '{print NF}'`
   if [ "$Arcnfld" -lt 3 ] ; then
    # Minimum format: uxxx_datetimestamp_arc 
    echo "arc_fnm: Sorry, not able to proceed. $arcfile must consist of at least of 3 fields!" ; echo ""
    if [ -n "$Prevwd" -a -n "$Tmpcmcarcdir" ] ; then
     # If applicable, cleanup temporary subdirectory and switch back
     # to previous subdirectory's path ... 
     cd $Prevwd
     rm -r -f $Tmpcmcarcdir
    fi
    exit 1
   fi

else
   arcfile_list=".${arcfile}"

   Arcnfld=`echo $arcfile | $AWK -F '_' '{print NF}'`
   if [ "$Arcnfld" -lt 3 ] ; then
    # Minimum format: identifier_datetimestamp_arc 
    echo "arc_fnm: Sorry, not able to proceed. $arcfile must consist of at least of 3 fields!" ; echo ""
    if [ -n "$Prevwd" -a -n "$Tmpcmcarcdir" ] ; then
     # If applicable, cleanup temporary subdirectory and switch back
     # to previous subdirectory's path ... 
     cd $Prevwd
     rm -r -f $Tmpcmcarcdir
    fi
    exit 1
   fi
fi
echo "arc_fnm: What will be used: arcfile=$arcfile, arcfile_list=$arcfile_list"
# echo "arc_fnm exit:"
