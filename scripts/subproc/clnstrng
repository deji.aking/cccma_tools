#! /bin/sh

#    Dec 22/97 - F.Majaess

#id  clnstrng- Used to eliminate duplicates from a string.

#    AUTHOR  - F.Majaess

#hd  PURPOSE - "clnstrng" script is used to eliminate duplicates 
#hd            from a string "arg1" with the substring separator
#hd            used passed in "arg2" and possibly a special character
#hd            passed via "arg3" to use internally to preserve the 
#hd            blank characters.
#hd            NOTE: original usage is to eliminate redundant arguments
#hd                  from the path.

#pr  PARAMETERS:
#pr
#pr    PRIMARY
#pr
#pr      arg1   = string to be cleaned 
#pr      arg2   = substring separator 
#pr      arg3   = character to use for preserving blanks (if applicable)
#pr               (any character not occuring in the string)

#ex  EXAMPLE:
#ex
#ex cleaned path listed in the output file: 
#ex
#ex          clnstrng "$PATH" ':' 
#ex
#ex duplicates eliminated from the path:
#ex
#ex          PATH=`clnstrng "$PATH" ':'`
#ex
#ex preserve blanks in the string:
#ex
#ex          clnstrng "ab cdef:abc def:abcdef:abc def" ':' '!'
#ex

set +xe
unset pathlist cpathlist cpathalist 
if [ $# -lt 2 ] ; then
 echo " clnstrng: Sorry, 2 arguments are needed!"
 exit 1
fi
pathlist="$1"
sprtrchr="$2"
if [ $# -gt 2 ] ; then
 prsrvchr="$3"
fi
prsrvchr=${prsrvchr:=' '}

# pathlist=`echo $pathlist | sed -n -e '/:/s/:/ /gp'`
pathlist=`echo $pathlist | sed -n -e "/ /s/ /$prsrvchr/g" -e '1,$p' `
pathlist=`echo $pathlist | sed -n -e "/$sprtrchr/s/$sprtrchr/ /g" -e '1,$p' `
if [ -n "$pathlist" ] ; then
  for arg in $pathlist
   do
    if [ -z "$cpathalist" ] ; then
      cpathlist="$arg"
      cpathalist="$arg"
    else
      dupl='no'
      for clndarg in $cpathalist
       do
        if [ "$arg" = "$clndarg" ] ; then
          dupl='yes'
#          echo "duplicated : $arg"
          break
        fi
       done
      if [ "$dupl" = 'no' ] ; then
       cpathlist="${cpathlist}${sprtrchr}${arg}"
       cpathalist="${cpathalist} ${arg}"
      fi
    fi
   done
fi
cpathlist=`echo $cpathlist | sed -n -e "s/$prsrvchr/ /g" -e '1,$p'`
echo "$cpathlist" 
unset pathlist cpathlist cpathalist
