#!/bin/sh
 
#    Feb 28/94 - E. Chan 

#id  fixup   - makes modifications to jobs, decks, and comdecks designed 
#id            for newdiag and converts them to newdiag2 format

#    AUTHOR  - E. Chan

#hd PURPOSE - 'fixup' scripts is used to make modifications to jobs, decks,
#hd            and comdecks designed for newdiag and converts them to 
#hd            newdiag2 format.

#pr  PARAMETERS:
#pr 
#pr    POSITIONAL
#pr 
#pr      fn1 fn2 ... fnm = list of m files that require fixing

#ex  EXAMPLE
#ex  
#ex   fixup *
#ex  
#ex   The above example fixes all files in the current directory. 

AWK=${AWK:-awk}

#  * Loop over all files in the input list.

for file
do

  # Ensure that only existing ascii text files are operated on.

  info=`file $file 2>/dev/null `
  file_status=`expr "$info" : '.*\(text\).*'`

  if [ ! -f "$file" ] ; then
    echo "file $file does not exist: resuming execution with next file in list"
  elif [ -z "$file_status" ] ; then
    echo "file $file is not an ascii text file: file skipped"
  else
    $AWK '
          { sub(/betapath/,"betapath2") }
          { sub(/^ *nojoin *= *on *$/,"join=1") }
          $0 ~ /^ *join[0-9]*=on/ {
            sub(/\=.*$/,"")
            sub(/join/,"join=")
          }
          $0 ~ /^#deck/ { dk = "on" }
          dk == "on" {
            if ( $0 ~ /^#!\/bin\/sh/ ) { next }
            if ( $0 ~ /\.  *setupdk/ ) { next }
            if ( $0 ~ /\. *id\.cdk/ ) { sub(/^.*$/,"#") }
            if ( $0 ~ /cat *> *execute/ ) { sub(/execute/,"Execute_Script") }
            if ( $0 ~ /read_data *> *Input_Cards/ ) { sub(/read_data/,"cat") }
          }
          $0 ~ /^#comdeck/ {
            cdk = "on"
            dk = "off"
          }
          cdk == "on" {
            if ( $0 ~ /read_data/ ) { next }
            if ( $0 ~ /end_of_data/ ) { next }
          }
          NR == 1 && dk != "on" && cdk != "on" { begin = "on" }
          $0 ~ /#end_of_job/ {
            begin_maybe = "on"
            print
            print ""
            next
          }
          $0 ~ /^ *#/ || $0 ~ /^ *\.  *betapath2/ {
            if ( begin_maybe == "on" ) { begin = "on" }
          }
          begin == "on" {
            begin = "off"
            begin_maybe = "off"
            print "#!/bin/sh"
            print "set -a"
            print ""
          }
          { print }
         '              $file > tmpfile$$
    mv tmpfile$$ $file
  fi 
done

exit
