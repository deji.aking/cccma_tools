#!/bin/sh
set -e

USAGE='
Compare 2 model files, variable by variable.

USAGE

  compare_model_files file1 file2

Examples:

  compare_model_files mc_test-run_5301_m01_gs.001 mc_rc3.1-pictrl_5301_m01_gs.001
'

if [ $# -ne 2 ] ; then
  echo "$USAGE"
  exit
fi

file1=$1
file2=$2
if [ ! -s $file1 ] ; then
  echo "ERROR: file1=$file1 is not found or empty."
  exit 1
fi
if [ ! -s $file2 ] ; then
  echo "ERROR: file2=$file2 is not found or empty."
  exit 1
fi
echo Comparing $file1 vs. $file2 ...

base1=`basename $file1`
base2=`basename $file2`

# temp directories (do it $RUNPATH)
pwd=`pwd`
wrkdir1=$RUNPATH/tmp.compare1.$base1.$$
wrkdir2=$RUNPATH/tmp.compare2.$base2.$$
mkdir -p $wrkdir1 $wrkdir2

# set a trap to cleanup on exit
trap "cleanup" 0 1 2 3 6 15
cleanup()
{
  exit_status=$?
  # echo exit_status=$exit_status
  # echo "Caught Signal ... cleaning up."
#  rm -rf $wrkdir1 $wrkdir2
}

# split 1st file
echo "Splitting file1=$file1 ..."
cd $wrkdir1
spltall $pwd/$file1 > /dev/null 2>&1

# split 2nd file
echo "Splitting file2=$file2 ..."
cd $wrkdir2
spltall $pwd/$file2 > /dev/null 2>&1

different=0
# compare each variable
for v in `cat .VARTABLE | cut -c1-5` ; do
  if [ "$v" = "PARM" -o "$v" = "PARC" ] ; then
    # skip PARM/PARC
    continue
  fi
  V=`echo $v | awk '{printf "%4s",$1}'`
  if [ -s $wrkdir1/$v -a -s $wrkdir2/$v ] ; then
    md5sum1=`md5sum $wrkdir1/$v | cut -f1 -d' '`
    md5sum2=`md5sum $wrkdir2/$v | cut -f1 -d' '`
    if [ "$md5sum1" != "$md5sum2" ] ; then
      echo "Variable v=$V is bit DIFFERENT."
      # echo "md5sum1=$md5sum1"
      # echo "md5sum2=$md5sum2"
      different=1
     # else
     #   echo "Variable v=$V is bit IDENTICAL."
    fi
  else
    echo "Variable v=$V does not exist in $file1."
  fi
done

if [ $different -eq 1 ] ; then
  echo "Files $file1 and $file2 are bit DIFFERENT!"
else
  echo "Files $file1 and $file2 are bit IDENTICAL!"
fi
cd $pwd
