#! /bin/sh
 
#    FEB 26/92 - F.Majaess
 
#id  lo      - Sends an "output queue" file to a remote printer.
 
#    AUTHOR  - F.Majaess
 
#hd  PURPOSE - "lo" script is used to dispose "fln" file from current machine
#hd            "ppath" subdirectory to a remote printer "un".
#hd            Note: a job is assembled and submitted directly to the 
#hd                  mainframe controlling the output printer in the 
#hd                  back-ground using the network connection (TCP/IP...).
 
#pr  PARAMETERS:
#pr
#pr    POSITIONAL     
#pr
#pr      fln    = local filename to be printed on "un" printer.
#pr
#pr    PRIMARY/SECONDARY
#pr
#pr      rmv    = switch controlling deletion of input file "fln"(=no/yes).
#pr      ppath  = path of subdirectory containing "fln" (=$OUTPUTQ/$PRNTQ).
#pr
#pr    PRIMARY
#pr
#pr      ncopies= number of copies requested (=1)
#pr      time   = back-end CPU time in seconds (=1)
#pr      mfl    = back-end MFL      in words   (=100000)
#pr      box    = box to dispose printout if desired (=$BOX).
#pr      un     = usernumber for printout (=$DESTIN). 
#pr      una    = user's account  (=$USER).
 
#ex  EXAMPLE: 
#ex
#ex    lo fln=output_queue_fln rmv un=ccrn 
  
#   * code used to set switches and deal with parameter=value arguments 
#   * as well as setting parameters to their primary/secondary defaults.
#   * The list of other arguments (if any) is returned in "prmtrl" var...
 
. $SUBPROC/check_set_swtches_prmtrs
 
#   * make sure arguments list variable "prmtrl" is initialized...
 
prmtrl=${prmtrl=}
  
#   * code used to set hard coded primary defaults...
 
eval "ncopies=${ncopies='1'}" 
eval "time=${time='1'}" 
eval "mfl=${mfl='100000'}"
 
#   * The following code deals with checking/setting positional 
#   * parameters ...
#   * Note: script will abort if too many arguments are specified.
 
if [ -n "$prmtrl" ] ; then
  i=1
  for prmtr in $prmtrl
  do
    case $i in
      1) fln=$prmtr;;
      *) eval "echo '$0 : too many parameters !' " ; exit ;;
    esac
    i=`expr $i + 1`
  done
fi
 
#   * Prompt for a positional parameter value(s) if none was/were
#   * specified...
 
while [ -z "$fln" ] ; do
  echo "please enter local filename  > \\c"
  read fln
done
 
#   ****   Task of the script...   ****
 
#   * check if a valid filename is specified, and call router script...
  
fln="$ppath/$fln"
if [ -f $fln -a -s $fln -a -r $fln ] ; then
  eval "$SUBPROC/rprint fln=$fln Tcaller=LO \\
	rmv=$rmv ncopies=$ncopies time=$time mfl=$mfl \\
	box=$box un=$un una=$una "
else
  eval "echo $fln file is not valid !"
  exit 1
fi
