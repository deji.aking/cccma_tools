#!/bin/sh

#    Dec 05/06 - F. Majaess (Added support for "source_asis" code)
#    Sep 21/04 - F. Majaess (Revised to include targeting "sv08src" 
#                            in Victoria)
#    Jun 23/04 - F. Majaess (Added scan of CCRNSRC/info/samples_dir)
#    Mar 03/94 - E. Chan

#id findcode - Searches all group accessible subdirectories under 
#id            under $CCRNSRC/source$OSbin on the server for the specified
#id            filename and returns the absolute path(s) to the source file.

#    AUTHOR  - E. Chan

#hd  PURPOSE - "findcode" performs a recursive search through all 
#hd            subdirectories under $CCRNSRC/source$OSbin for the specified
#hd            filename and returns the absolute path(s) of all matches.
#hd 
#hd            The specified filename need not be exact. All paths are
#hd            returned for files that *begin* with the given filename.

#pr  PARAMETERS:
#pr 
#pr    POSITIONAL
#pr 
#pr        fil  = name of subroutine, function, program, or deck
#pr 

#ex  EXAMPLE:
#ex 
#ex   1) findcode ingcm
#ex 
#ex      The above example returns the paths to all source code files
#ex      with filenames beginning with "ingcm"

#  * Set variable to the version of "awk" to be used. The original version
#  * of 'awk' does not have the capabilities required for this script
#  * and 'nawk' must be used instead. Fortunately, it seems that on most
#  * systems, 'awk' is actually 'nawk'. On systems where this is not the
#  * case, the variable AWK must be set to nawk.

AWK=${AWK:-awk}

#  * Obtain the file names and any specified option.

for arg
do
  case $arg in
      -*) set $arg                                  ;;
       *) fil=${fil:-$arg}
  esac
done 

if [ -z "$fil" ] ; then
  echo "Error: must specify a filename (or at least a string which"
  echo "       can be used to match with filenames beginning with"
  echo "       the string)"
  exit 1
fi

fil=`echo $fil | tr '[A-Z]' '[a-z]'`

find -L $CCRNSRC/source${OSbin}/. -name "$fil*" -print 2>/dev/null
if [ -d "$CCRNSRC/info/samples_dir/." ] ; then
 find -L $CCRNSRC/info/samples_dir/. -name "$fil*" -print 2>/dev/null
fi
if [ -d "$CCRNSRC/source_asis/." ] ; then
 find -L $CCRNSRC/source_asis/. -name "$fil*" -print 2>/dev/null
fi
if [ "$SITE_ID" = 'Victoria' ] ; then 
 if [ -n "$OSbin" -a -d "$CCRNSRC/sv08src/source${OSbin}/." ] ; then
  find -L $CCRNSRC/sv08src/source${OSbin}/. -name "$fil*" -print 2>/dev/null
 elif [ -d "$CCRNSRC/sv08src/source/." ] ; then
  find -L $CCRNSRC/sv08src/source/. -name "$fil*" -print 2>/dev/null
 fi
fi

exit
