#! /bin/sh
 
#    JAN 09/92 - F.Majaess
 
#id  qg      - Allows inspection and disposition of job output.
 
#    AUTHOR  - F.Majaess
 
#hd  PURPOSE - "qg" script is used to retrieve (job) output contained
#hd            in "fln" file in "ppath" subdirectory for inpection.
#hd            "fln" file postprocessing is decided by procedure 
#hd            "qgdest".
 
#pr  PARAMETERS:
#pr
#pr    POSITIONAL     
#pr
#pr      fln    = (job) output local filename to be handled by "qg".
#pr
#pr    PRIMARY/SECONDARY
#pr
#pr      ppath  = path of subdirectory containing "fln" (=$OUTPUTQ/$PRNTQ).
#pr
#pr    PRIMARY
#pr
#pr      vwr    = user's preferred viewer utility (=$PAGER).
 
#ex  EXAMPLE: 
#ex
#ex    qg fln=output_queue_fln 
  
#   * code used to set switches and deal with parameter=value arguments 
#   * as well as setting parameters to their primary/secondary defaults.
#   * The list of other arguments (if any) is returned in "prmtrl" var...
 
. $SUBPROC/check_set_swtches_prmtrs
 
cd $ppath 

#   * make sure arguments list variable "prmtrl" is initialized...
 
prmtrl=${prmtrl=}
  
#   * code used to set hard coded primary defaults...
 
eval "vwr=${vwr=$PAGER}"
 
#   * The following code deals with checking/setting positional 
#   * parameters ...
#   * Note: script will abort if too many arguments are specified.
 
if [ -n "$prmtrl" ] ; then
  i=1
  for prmtr in $prmtrl
  do
    case $i in
      1) fln=$prmtr;;
      *) eval "echo '$0 : too many parameters !' " ; exit ;;
    esac
    i=`expr $i + 1`
  done
fi
 
#   * Prompt for a positional parameter value(s) if none was/were
#   * specified...
 
while [ -z "$fln" ] ; do
  echo "please enter filename to be processed  > \\c"
  read fln
done
 
#   ****   Task of the script...   ****
 
#   * check if a valid filename is specified, and process as requested ...
  
prmtrl=$fln

for fln in $prmtrl
do

if [ -f $ppath/$fln -a -s $ppath/$fln -a -r $ppath/$fln ] ; then
 
  case $vwr in
   more|vi ) eval "$vwr $ppath/$fln " ;;
   ed      ) eval "$vwr -s $ppath/$fln " ;;
   *       ) eval "$vwr $ppath/$fln " ;;
  esac
  Tprmtrl=
  Tprmtr=" "
  while [ -n "$Tprmtr" ] ; do
    Tprmtrl="$Tprmtrl $Tprmtr"
    echo "for $ppath/$fln file "
    echo "please enter parameters for qgdest besides fln and ppath : "
    read Tprmtr
  done
  echo " additional paramters for qgdest: "
  echo " $Tprmtrl "
  eval "$SUBPROC/qgdest ppath=$ppath $fln $Tprmtrl "
fi
done
