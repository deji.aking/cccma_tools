#!/bin/sh
#=======================================================================
# Delete seasonal files from DATAPATH/RUNPATH            --- sdelete ---
# $Id: sdelete_jobdef 656 2011-12-28 19:51:43Z acrnrls $
#=======================================================================
#
# files deleted will be of the form
#    ${sdelete_prefix}_${year}_${season}_$suffix
# for all seasons from previous_year, previous_month to current_year,
# current_month or from current_year, current_month to next_year,
# next_month depending on which of previous_(year|month) or
# next_(year|month) are set.
# The default suffix list is "gp xp" which means that gp and xp files
# are deleted each season
#
# The variables current_year, current_month, previous_year and
# previous_month are set when the job string is created.
#
# explicitly setting sdelete_start_year overrides previous_year
# explicitly setting sdelete_start_mon  overrides previous_month
# explicitly setting sdelete_stop_year   overrides current_year
# explicitly setting sdelete_stop_mon    overrides current_month
#
# A variable named sdelete_suffix_list may be set to modify the list
# of files that will be deleted. See below for details.
#
# sdelete_prefix_ can be set to override the value of sdelete_prefix
# files deleted will then be of the form
#    ${sdelete_prefix_}${year}_${season}_$suffix
# note the missing underscore between sdelete_prefix_ and year.
#
# sdelete_djf, sdelete_mam, sdelete_jja, sdelete_son and sdelete_ann
# may be set to flag the inclusion of individual seasonal or annual
# average files.
#=======================================================================
#
#     keyword :: sdelete
# description :: delete seasonal and/or annual averaged files
#

  set -a
  . betapath2

#  * ........................... Parmsub Parameters ............................

  runid="job000"; uxxx='uxxx'; pool_uxxx=$uxxx; sdelete_uxxx=$pool_uxxx;
  jobname=sdelete; sdelete_prefix="${sdelete_uxxx}_${runid}"; sdelete_prefix_=${sdelete_prefix}_
  crawork="${runid}_job"; username="acrnxxx"; user="XXX";
  nqsprfx="${runid}_"; nqsext='';

  stime="1800"; memory1="250mb"; lopgm="lopgm";

  # Allow the user to reset CCRNTMP and/or RUNPATH
  pool_RUNPATH=''
  RUNPATH=${pool_RUNPATH:=$RUNPATH}
  pool_CCRNTMP=''
  CCRNTMP=${pool_CCRNTMP:=$CCRNTMP}

  # Alternate path to a directory where .queue/.crawork will be found
  JHOME=''

  if [ -n "$JHOME" -a x"$JHOME" != x"$HOME" ]; then
    # Allow optional reset of DATAPATH/RUNPATH
    JHOME_DATA=''
    DATAPATH=${JHOME_DATA:=$DATAPATH}
    RUNPATH=${JHOME_DATA:=$RUNPATH}
    # Allow optional reset of CCRNTMP
    JHOME_RUN=''
    CCRNTMP=${JHOME_RUN:=$CCRNTMP}
  fi

  # ---Start_submit_ignore_code----

  stamp=`date "+%j%H%M%S"$$`

  # Use -e option if recognized by echo
  if [ "X`echo -e`" = "X-e" ]; then
    echo_e() { echo ${1+"$@"}; }
  else
    echo_e() { echo -e ${1+"$@"}; }
  fi

  # bail is a simple error exit routine
  # Note: we write the error directly to a file in ~/.queue so that this
  #       info is not lost if/when stdout is not returned
  error_out="${JHOME:-$HOME}/.queue/error_sdelete_${runid}_$stamp"
  [ ! -z "$error_out" ] && rm -f $error_out
  bail(){
    echo_e `date`" --- sdelete: $*"
    echo_e `date`" --- sdelete: $*" >>$error_out
    exit 1
  }

  # These variables are set when the job string is created

  previous_year=NotSet
  previous_month=NotSet

  current_year=NotSet
  current_month=NotSet

  next_year=NotSet
  next_month=NotSet

  run_start_year=NotSet
  run_start_month=NotSet
  run_stop_year=NotSet
  run_stop_month=NotSet

  # Pooling start and stop dates
  # These may differ from run start and stop dates, but are the same by default
  pool_start_year=$run_start_year
  pool_start_month=$run_start_month
  pool_stop_year=$run_stop_year
  pool_stop_month=$run_stop_month

  # If either reset_start_year or reset_stop_year are set then they must be
  # of the form old_year:new_year (ie a colon separated pair of integers)
  # where the first integer is the year that needs to be changed
  # and the second integer is the year that it will be changed to.
  # These may potentially change the value of start_year or stop_year that
  # are defined after the call to make_file_name_list below
  sdelete_reset_start_year=''
  reset_start_year=${sdelete_reset_start_year:=''}
  sdelete_reset_end_year=''
  sdelete_reset_stop_year=$sdelete_reset_end_year
  reset_stop_year=${sdelete_reset_stop_year:=''}

  # This invocation of make_file_name_list will process the *_year and *_months
  # variables defined above and output a file containing definitions for
  # start_year, start_mon, stop_year, stop_mon
  tmp_file_list="${JHOME:-$HOME}/tmp/sdelete_date_list_${runid}_${stamp}"
  make_file_name_list --dates_only $tmp_file_list >>$error_out 2>&1 ||\
    bail "Problem in make_file_name_list"
  rm -f $error_out

  # Verify that the output list is not empty
  [ ! -s "$tmp_file_list" ] && bail "Unable to create file list"

  # A file list was created ...source it
  # This will define start_year, start_mon, stop_year, stop_mon
  : ; . $tmp_file_list
  rm -f $tmp_file_list

  # Define start and stop dates
  sdelete_start_year=$start_year
  sdelete_start_mon=$start_mon
  sdelete_stop_year=$stop_year
  sdelete_stop_mon=$stop_mon

 # sdelete_suffix_list is a white space separated list of suffixes
 # of file names to be generated here. Any suffix in this list may be
 # modified by appending a + followed by a comma separated list of
 # numbers (no white space is allowed within this modifier). Each
 # number within the modifier list will correspond to a month (1-12)
 # for which a file with this suffix is to be included. If the
 # modifier exists for a particular suffix then only those months
 # indicated in the modifier will be added to the file list.
 sdelete_suffix_list='gp xp'

 # sdelete_suffix_ flags the addition of an underscore between the
 # suffix and the month (e.g. djf_gs) in file names generated here
 sdelete_suffix_=1
 XXX=`echo $sdelete_suffix_|sed 's/ //g'`
 eval sdelete_suffix_\=$XXX
 [ "$sdelete_suffix_" = 'on' ]  && eval sdelete_suffix_\=1
 [ "$sdelete_suffix_" = 'off' ] && eval sdelete_suffix_\=0
 [ "$sdelete_suffix_" = 'yes' ] && eval sdelete_suffix_\=1
 [ "$sdelete_suffix_" = 'no' ]  && eval sdelete_suffix_\=0

 # sdelete_pooled flags deleting pooled seasons rather than individual
 # seasons. The difference is that file names for pooled files are
 # of the form e.g.
 # ${sdelete_prefix}_${sdelete_start_year}_${sdelete_stop_year}_djf_gp
 # ${sdelete_prefix}_${sdelete_start_year}_${sdelete_stop_year}_djf_xp
 sdelete_pooled=0
 XXX=`echo $sdelete_pooled|sed 's/ //g'`
 eval sdelete_pooled\=$XXX
 [ "$sdelete_pooled" = 'on' ]  && eval sdelete_pooled\=1
 [ "$sdelete_pooled" = 'off' ] && eval sdelete_pooled\=0
 [ "$sdelete_pooled" = 'yes' ] && eval sdelete_pooled\=1
 [ "$sdelete_pooled" = 'no' ]  && eval sdelete_pooled\=0

 # sdelete_djf, sdelete_mam, sdelete_jja and sdelete_son flag the inclusion of
 # seasonal average files determined as file names of the form 
 # ${sdelete_prefix_}${year}_${season}_$suffix
 # for season = djf, mam, jja or son
 sdelete_djf=1
 XXX=`echo $sdelete_djf|sed 's/ //g'`
 eval sdelete_djf\=$XXX
 [ "$sdelete_djf" = 'on' ]  && eval sdelete_djf\=1
 [ "$sdelete_djf" = 'off' ] && eval sdelete_djf\=0
 [ "$sdelete_djf" = 'yes' ] && eval sdelete_djf\=1
 [ "$sdelete_djf" = 'no' ]  && eval sdelete_djf\=0
 sdelete_mam=1
 XXX=`echo $sdelete_mam|sed 's/ //g'`
 eval sdelete_mam\=$XXX
 [ "$sdelete_mam" = 'on' ]  && eval sdelete_mam\=1
 [ "$sdelete_mam" = 'off' ] && eval sdelete_mam\=0
 [ "$sdelete_mam" = 'yes' ] && eval sdelete_mam\=1
 [ "$sdelete_mam" = 'no' ]  && eval sdelete_mam\=0
 sdelete_jja=1
 XXX=`echo $sdelete_jja|sed 's/ //g'`
 eval sdelete_jja\=$XXX
 [ "$sdelete_jja" = 'on' ]  && eval sdelete_jja\=1
 [ "$sdelete_jja" = 'off' ] && eval sdelete_jja\=0
 [ "$sdelete_jja" = 'yes' ] && eval sdelete_jja\=1
 [ "$sdelete_jja" = 'no' ]  && eval sdelete_jja\=0
 sdelete_son=1
 XXX=`echo $sdelete_son|sed 's/ //g'`
 eval sdelete_son\=$XXX
 [ "$sdelete_son" = 'on' ]  && eval sdelete_son\=1
 [ "$sdelete_son" = 'off' ] && eval sdelete_son\=0
 [ "$sdelete_son" = 'yes' ] && eval sdelete_son\=1
 [ "$sdelete_son" = 'no' ]  && eval sdelete_son\=0

 # sdelete_ann flags the inclusion of annual average files determined
 # as file names with ann replacing djf, mam, jja or son.
 sdelete_ann=1
 XXX=`echo $sdelete_ann|sed 's/ //g'`
 eval sdelete_ann\=$XXX
 [ "$sdelete_ann" = 'on' ]  && eval sdelete_ann\=1
 [ "$sdelete_ann" = 'off' ] && eval sdelete_ann\=0
 [ "$sdelete_ann" = 'yes' ] && eval sdelete_ann\=1
 [ "$sdelete_ann" = 'no' ]  && eval sdelete_ann\=0

 # sdelete_mon flags the inclusion of monthly average files
 # file names with m01,m02,... replacing djf, mam, jja or son.
 sdelete_mon=0
 XXX=`echo $sdelete_mon|sed 's/ //g'`
 eval sdelete_mon\=$XXX
 [ "$sdelete_mon" = 'on' ]  && eval sdelete_mon\=1
 [ "$sdelete_mon" = 'off' ] && eval sdelete_mon\=0
 [ "$sdelete_mon" = 'yes' ] && eval sdelete_mon\=1
 [ "$sdelete_mon" = 'no' ]  && eval sdelete_mon\=0

 # force_first_djf flags the inclusion of a DJF delete during run_start_year
 # Note: To delete force_first_djf must be either 1, 'on' or 'yes'.
 force_first_djf=0
 XXX=`echo $force_first_djf|sed 's/ //g'`
 eval force_first_djf\=$XXX
 [ "$force_first_djf" = 'on' ]  && eval force_first_djf\=1
 [ "$force_first_djf" = 'off' ] && eval force_first_djf\=0
 [ "$force_first_djf" = 'yes' ] && eval force_first_djf\=1
 [ "$force_first_djf" = 'no' ]  && eval force_first_djf\=0

 # force_first_sea flags pooling the first season in the string
 # even if pool_start_(year|month) falls after the first month of that season
 force_first_sea=off
 XXX=`echo $force_first_sea|sed 's/ //g'`
 eval force_first_sea\=$XXX
 [ "$force_first_sea" = 'on' ]  && eval force_first_sea\=1
 [ "$force_first_sea" = 'off' ] && eval force_first_sea\=0
 [ "$force_first_sea" = 'yes' ] && eval force_first_sea\=1
 [ "$force_first_sea" = 'no' ]  && eval force_first_sea\=0

 # Ensure that force_first_djf is true when force_first_sea is true
 [ $force_first_sea -eq 1 ] && eval force_first_djf\=1

 # pool_range is used as part of pooled file names
 pool_range_first_sea=${sdelete_start_year}_${sdelete_stop_year}

 # Generate a list of history files
 sdelete_curr_year=`echo $sdelete_start_year|awk '{y=$1-1;printf "%04d", y}' -`
 join=0
 while [ $sdelete_curr_year -lt $sdelete_stop_year ]; do
   sdelete_curr_year=`echo $sdelete_curr_year|awk '{y=1+$1;printf "%04d", y}' -`
   if [ $sdelete_curr_year -eq $sdelete_start_year ]; then
     mm=`echo $sdelete_start_mon|awk '{m=$1-1;printf "%02d", m}' -`
   else
     mm=0
   fi
   if [ $sdelete_curr_year -eq $sdelete_stop_year ]; then
     mm_end=$sdelete_stop_mon
   else
     mm_end=12
   fi

   if [ $sdelete_pooled -eq 1 ]; then
     # When dumping pooled files iterate only once through the year loop
     if [ $sdelete_curr_year -eq $sdelete_start_year ]; then
       # Ensure that the month loop interates over all months
       mm=0
       mm_end=12
       force_first_djf=1
     else
       continue
     fi
   fi

   while [ $mm -lt $mm_end ]; do
     mm=`echo $mm|awk '{m=1+$1;printf "%02d", m}' -`
     # Reinitialize pool_range on each loop iteration
     pool_range=$pool_range_first_sea
     case $mm in
       02) # Feb ...delete DJF
           if [ "$sdelete_djf" = '1' ]; then
             if [ $run_start_year -eq $sdelete_curr_year ]; then
               if [ $force_first_djf -eq 1 ]; then
                 # Force a delete of the first DJF
                 # The user must set force_first_djf to indicate that
                 # the DEC files from the previous year are present
                 season='djf'
               else
                 continue
               fi
             else
               season='djf'
             fi
           else
             continue
           fi
           ;;
       05) # May ...delete MAM
           if [ "$sdelete_mam" = '1' ]; then
             if [ $run_start_year -eq $sdelete_curr_year -a \
                  $run_start_month -ge 3 ]; then
               if [ $force_first_sea -eq 1 ]; then
                 # Force a delete of the first MAM
                 season='mam'
               else
                 continue
               fi
             else
               season='mam'
             fi
           else
             continue
           fi
           ;;
       08) # Aug ...delete JJA
           if [ "$sdelete_jja" = '1' ]; then
             if [ $run_start_year -eq $sdelete_curr_year -a \
                  $run_start_month -ge 6 ]; then
               if [ $force_first_sea -eq 1 ]; then
                 # Force a delete of the first JJA
                 season='jja'
               else
                 continue
               fi
             else
               season='jja'
             fi
           else
             continue
           fi
           ;;
       11) # Nov ...delete SON
           if [ "$sdelete_son" = '1' ]; then
             if [ $run_start_year -eq $sdelete_curr_year -a \
                  $run_start_month -ge 9 ]; then
               if [ $force_first_sea -eq 1 ]; then
                 # Force a delete of the first SON
                 season='son'
               else
                 continue
               fi
             else
               season='son'
             fi
           else
             continue
           fi
           ;;
       12) # Dec ...delete annual average
           if [ "$sdelete_ann" = '1' ]; then
             season='ann'
           else
             continue
           fi
           ;;
       *) continue ;;
     esac
     if [ "$sdelete_suffix_" = '1' ]; then
       if [ "$sdelete_pooled" = '1' ]; then
         # use a different form for file names when dumping pooled files
         bname=${sdelete_prefix_}${pool_range}_${season}_
       else
         bname=${sdelete_prefix_}${sdelete_curr_year}_${season}_
       fi
     else
       if [ "$sdelete_pooled" = '1' ]; then
         # use a different form for file names when dumping pooled files
         bname=${sdelete_prefix_}${pool_range}_$season
       else
         bname=${sdelete_prefix_}${sdelete_curr_year}_$season
       fi
     fi
     for suffix in $sdelete_suffix_list; do
       mlist=`echo $suffix|awk -F'+' '{print $2}' -`
       mlist=`echo $mlist|sed 's/,/ /g'`
       if [ -n "$mlist" ]; then
         suffix=`echo $suffix|sed 's/+.*$//'`
         # assume that mlist is a white space separated list of numbers
         # indicating which months to delete
         for xx in $mlist; do
           if [ $xx -eq $mm ]; then
             join=`echo $join|awk '{j=1+$1;printf "%d",j}' -`
             join=`echo $join|sed -e 's/^ *//' -e 's/^0*//'`
             eval file${join}=$bname$suffix
           fi
         done
       else
         join=`echo $join|awk '{j=1+$1;printf "%d",j}' -`
         join=`echo $join|sed -e 's/^ *//' -e 's/^0*//'`
         eval file${join}=$bname$suffix
       fi
     done

   done
 done

 # Add monthly files to the list if requested
 sdelete_leave_last_mon=1
 XXX=`echo $sdelete_leave_last_mon|sed 's/ //g'`
 eval sdelete_leave_last_mon\=$XXX
 [ "$sdelete_leave_last_mon" = 'on' ]  && eval sdelete_leave_last_mon\=1
 [ "$sdelete_leave_last_mon" = 'off' ] && eval sdelete_leave_last_mon\=0
 [ "$sdelete_leave_last_mon" = 'yes' ] && eval sdelete_leave_last_mon\=1
 [ "$sdelete_leave_last_mon" = 'no' ]  && eval sdelete_leave_last_mon\=0
 if [ $sdelete_leave_last_mon -eq 1 -a $sdelete_pooled -eq 0 ]; then
   # Leave the last month on disk by shifting these dates back 1 month
   sdelete_vtmp=`echo $sdelete_start_year $sdelete_start_mon|\
                  awk '{m=$2-1;if(m==0){m=12;y=$1-1}else{y=$1};
                        printf "%02d %04d",m,y}' -`
   sdelete_start_mon=`echo $sdelete_vtmp|awk '{printf "%02d",$1}' -`
   sdelete_start_year=`echo $sdelete_vtmp|awk '{printf "%04d",$2}' -`

   sdelete_vtmp=`echo $sdelete_stop_year $sdelete_stop_mon|\
                  awk '{m=$2-1;if(m==0){m=12;y=$1-1}else{y=$1};
                        printf "%02d %04d",m,y}' -`
   sdelete_stop_mon=`echo $sdelete_vtmp|awk '{printf "%02d",$1}' -`
   sdelete_stop_year=`echo $sdelete_vtmp|awk '{printf "%04d",$2}' -`
 fi
 if [ "$sdelete_mon" = '1' ]; then
   sdelete_curr_year=`echo $sdelete_start_year|awk '{y=$1-1;printf "%04d", y}' -`
   while [ $sdelete_curr_year -lt $sdelete_stop_year ]; do
     sdelete_curr_year=`echo $sdelete_curr_year|awk '{y=1+$1;printf "%04d", y}' -`
     if [ $sdelete_curr_year -eq $sdelete_start_year ]; then
       mm=`echo $sdelete_start_mon|awk '{m=$1-1;printf "%02d", m}' -`
     else
       mm=0
     fi
     if [ $sdelete_curr_year -eq $sdelete_stop_year ]; then
       mm_end=$sdelete_stop_mon
     else
       mm_end=12
     fi

     if [ $sdelete_pooled -eq 1 ]; then
       # When dumping pooled files iterate only once through the year loop
       if [ $sdelete_curr_year -eq $sdelete_start_year ]; then
         # Ensure that the month loop interates over all months
         mm=0
         mm_end=12
       else
         continue
       fi
     fi

     while [ $mm -lt $mm_end ]; do
       mm=`echo $mm|awk '{m=1+$1;printf "%02d", m}' -`
       if [ "$sdelete_suffix_" = '1' ]; then
         if [ "$sdelete_pooled" = '1' ]; then
           # use a different form for file names when dumping pooled files
           bname=${sdelete_prefix_}${sdelete_start_year}_${sdelete_stop_year}_m${mm}_
         else
           bname=${sdelete_prefix_}${sdelete_curr_year}_m${mm}_
         fi
       else
         if [ "$sdelete_pooled" = '1' ]; then
           # use a different form for file names when dumping pooled files
           bname=${sdelete_prefix_}${sdelete_start_year}_${sdelete_stop_year}_m$mm
         else
           bname=${sdelete_prefix_}${sdelete_curr_year}_m$mm
         fi
       fi
       for suffix in $sdelete_suffix_list; do
         mlist=`echo $suffix|awk -F'+' '{print $2}' -`
         mlist=`echo $mlist|sed 's/,/ /g'`
         if [ -n "$mlist" ]; then
           suffix=`echo $suffix|sed 's/+.*$//'`
           # assume that mlist is a white space separated list of numbers
           # indicating which months to dump
           for xx in $mlist; do
             if [ $xx -eq $mm ]; then
               join=`echo $join|awk '{j=1+$1;printf "%d",j}' -`
               join=`echo $join|sed -e 's/^ *//' -e 's/^0*//'`
               eval file${join}=$bname$suffix
             fi
           done
         else
           join=`echo $join|awk '{j=1+$1;printf "%d",j}' -`
           join=`echo $join|sed -e 's/^ *//' -e 's/^0*//'`
           eval file${join}=$bname$suffix
         fi
       done
     done
   done
 fi

 sdelete_leave_last_pool=1
 XXX=`echo $sdelete_leave_last_pool|sed 's/ //g'`
 eval sdelete_leave_last_pool\=$XXX
 [ "$sdelete_leave_last_pool" = 'on' ]  && eval sdelete_leave_last_pool\=1
 [ "$sdelete_leave_last_pool" = 'off' ] && eval sdelete_leave_last_pool\=0
 [ "$sdelete_leave_last_pool" = 'yes' ] && eval sdelete_leave_last_pool\=1
 [ "$sdelete_leave_last_pool" = 'no' ]  && eval sdelete_leave_last_pool\=0
 if [ $sdelete_leave_last_pool -eq 1 -a $sdelete_pooled -eq 1 ]; then
   year_bound=`echo $sdelete_start_year $sdelete_stop_year $run_stop_year |\
                 awk '{b=$3-$2+$1;printf "%d",b}' -`
   if [ $sdelete_stop_year -ge $year_bound ]; then
     # Do not delete any files when sdelete_stop_year lies within
     # the pooling range of the end of the run
     # This is accomplished by supplying a single file name
     # that is unlikely to exist on DATAPATH/RUNPATH
     join=1
     file1='s_____w____x___93475__y_z'
   fi
 fi

 [ $join -eq 0 ] &&
   bail "No files selected for the year/mon range $sdelete_start_year/$sdelete_start_mon to $sdelete_stop_year/$sdelete_stop_mon"

 # ---Stop_submit_ignore_code----

#  * ............................ Condef Parameters ............................

 noprint=on
 nextjob=on

#  * ............................. Deck Definition .............................

 # Note: If delet is placed inside a conditional then submit3 will fail.
 # submit3 will ignore everthing after the delet line when it creates an
 # internal file named job.info. It then attempts to source job.info and dies
 # due to a syntax error (the final fi is missing).
 . delet.dk


#end_of_job

