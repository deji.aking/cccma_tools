#!/bin/bash
#     keyword :: canesm_load_ncdf
# description :: Load netcdf time series files.
set -a
. betapath2  # sets path for new package (required until old package is retired)

#  * ........................... Parmsub Parameters ............................

 runid="runid"; year="yyyy"; mon="mm"; run_start_year="yyy";run_start_month="mm";
 ncdf_uxxx="nc";

  # These variables are set when the job string is created
  previous_year=NotSet
  previous_month=NotSet

  current_year=NotSet
  current_month=NotSet

  next_year=NotSet
  next_month=NotSet

  run_start_year=NotSet
  run_start_month=NotSet
  run_stop_year=NotSet
  run_stop_month=NotSet

 crawork="crawork";
 username="username"; user="USER"; nqsprfx=""; nqsext="_${runid}_${year}_m${mon}";
 lopgm="lopgm"; stime="10800"; memory1="4gb";

#  * ............................ Condef Parameters ............................

 hpcarc_user=unknown
 noprint=on
 nextjob=on
 debug=off
 shortermdir=$canesm_dump_nc_short_term

#  * ............................. Deck Definition .............................
jobname=load_${ncdf_uxxx}; time=$stime ; memory=$memory1
.   comjcl.cdk
cat > Execute_Script <<'end_of_script'
# Loads netcdf files from HPNLS using hpcarchive
# RSK, 06 2019

# load using hpcarchive
if [ "$shortermdir" = "on" ] ; then
  hpcarchive_project=crd_short_term
else
  hpcarchive_project=crd_cccma
fi

# find the latest archive
if [ "$hpcarc_user" = "unknown" -o "$hpcarc_user" = "" ] ; then
  hpcarc_user_arg="-U"
else
  hpcarc_user_arg="-u $hpcarc_user"
fi
hpcarchive_pfx=${ncdf_uxxx}_${runid}_${previous_year}${previous_month}_${current_year}${current_month}_
hpcarchive=`hpcarchive -p $hpcarchive_project $hpcarc_user_arg -L -x -s -c "^$hpcarchive_pfx" | grep $hpcarchive_pfx | head -1`
if [ "$hpcarchive" = "" ] ; then
  # try a more general regex used for older runs
  hpcarchive_pfx=${ncdf_uxxx}_${runid}_${current_year}_
  hpcarchive=`hpcarchive -p $hpcarchive_project $hpcarc_user_arg -L -x -s -c "^$hpcarchive_pfx" | grep $hpcarchive_pfx | head -1`
  if [ "$hpcarchive" = "" ] ; then
    echo "ERROR: no HPC archive is found with this prefix: $hpcarchive_pfx!"
    exit 1
  fi
fi
hpcarcuser=`echo $hpcarchive | cut -f1 -d' ' | cut -c2-`
hpcarcname=`echo "$hpcarchive" | sed "s/.* \(${hpcarchive_pfx}[0-9]\+\)/\1/"`

# load full archive - try several times
ntrymax=10
ntry=1
while [ $ntry -le $ntrymax ] ; do
  status=""
  hpcarchive -p ${hpcarchive_project} -u ${hpcarcuser} -r -c ${hpcarcname} || status=$?
  echo status=$status
  if [ "$status" = "" ] ; then
    break
  fi
  # unsuccessful retrieval - sleep a bit and try again.
  rm -rf ${hpcarcname}
  sleep 60
  ntry=`expr $ntry + 1`
done
if [ "$status" != "" ] ; then
  exit 1
fi

# rsync files
cd ${hpcarcname}
rsync -avP CMIP6 $RUNPATH/nc_output

end_of_script

. endjcl.cdk
#end_of_job
