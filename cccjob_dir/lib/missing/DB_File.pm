# This is a stub for DB_File to be used to allow compilation on
# machines that do not have DB_File installed 

package DB_File::NOT_AVAILABLE ;
use strict ;
require Tie::Hash;
@DB_File::NOT_AVAILABLE::ISA = qw(Tie::Hash);

sub new
{
    my $pkg = shift ;
    my %x ;
    tie %x, $pkg ;
    bless \%x, $pkg ;
}

sub TIEHASH
{
    my $pkg = shift ;

    bless { VALID => { map {$_, 1}
                       qw( bval cachesize psize flags lorder reclen bfname )
                     },
            GOT   => {},
          }, $pkg ;
}

package DB_File ;

use strict;
use vars qw(@ISA @EXPORT $DB_BTREE $DB_HASH $DB_RECNO) ;

require Tie::Hash;
require Exporter ;
@ISA = qw(Exporter) ;
@EXPORT = qw($DB_BTREE $DB_HASH $DB_RECNO) ;

$DB_BTREE = new DB_File::NOT_AVAILABLE ;
$DB_HASH  = new DB_File::NOT_AVAILABLE ;
$DB_RECNO = new DB_File::NOT_AVAILABLE ;


eval {
    # Make all Fcntl O_XXX constants available for importing
    require Fcntl;
    my @O = grep /^O_/, @Fcntl::EXPORT;
    Fcntl->import(@O);  # first we import what we want to export
    push(@EXPORT, @O);
};

1;
