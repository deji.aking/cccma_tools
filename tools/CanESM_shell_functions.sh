# library of useful shell functions to be used in the CanESM sequencing
function bail(){
    >&2 echo " *** ERROR *** " 
    >&2 echo "  Calling script: $(basename ${BASH_SOURCE[-1]})"
    >&2 echo "  Function call stack: ${FUNCNAME[@]}"
    >&2 echo "  Error occurred in: ${FUNCNAME[1]}"
    >&2 echo "  Error: $1"
    exit 1
}
function is_defined(){
    local var
    local exit_status
    var=$1
    if [[ -z "$var" ]]; then
        exit_status=1
    else 
        exit_status=0
    fi
    return $exit_status
}
function is_file(){
    local obj
    local exit_status
    obj=$1
    if [[ -f "$obj" ]]; then
        exit_status=0
    else
        exit_status=1
    fi
    return $exit_status
}
function exists(){
    local obj
    local exit_status
    obj=$1
    if [[ -e "$obj" ]]; then
        exit_status=0
    else
        exit_status=1
    fi
    return $exit_status
}
function is_zero_size(){
    local obj
    local exit_status
    obj=$1
    if [[ -s "$obj" ]]; then
        # file ISN'T zero size - use non-zero exit status
        exit_status=1
    else
        # file IS zero size - use zero exit status
        exit_status=0
    fi
    return $exit_status
}
function is_directory(){
    local obj
    local exit_status
    obj=$1
    if [[ -d "$obj" ]]; then
        exit_status=0
    else
        exit_status=1
    fi
    return $exit_status
}

function strip_leading_zeros(){
    # function to strip leading zeros
    local number_to_strip
    number_to_strip=$1
    echo $number_to_strip | sed 's/^0*//'
}

function pad_integer(){
    # function to pad leading zeros
    local integer_to_pad
    local padded_length

    # first we remove any existing leading zeros so we don't confuse printf
    integer_to_pad=$( strip_leading_zeros $1 )
    padded_length=$2
    echo $(printf "%0${padded_length}d" $integer_to_pad)
}

function trim_whitespace(){
    # function to trim off leading/trailing whitespace
    local var
    local trimmed_var
    var=$1
    trimmed_var=$( echo $var | xargs )
    echo $trimmed_var
}

function is_in_namelist(){
    # boolean check to see if variable definition is present in
    # namelist file or not
    set -e
    local var
    local namelist_file
    local exit_status
    var=$1
    namelist_file=$2

    # begin by assuming variable is present (0 exit status)
    exit_status=0

    # perform silent grep to check for variable, flipping to non-zero exit status if not present
    #   - regex explanation:
    #       ^   => beginning of line
    #       \s* => 0+ whitespace characters (used on both sides of the given variable)
    grep -Eq "^\s*${var}\s*=" $namelist_file || exit_status=1
    return $exit_status
}

function get_namelist_var(){
    # extract namelist variable from the given namelist file
    set -e
    local var
    local var_value
    local definition_statement
    local namelist_file
    var=$1
    namelist_file=$2

    # first check if variable is in the file
    if ! is_in_namelist $var $namelist_file; then
        bail "$var is not present in $namelist_file!"
    fi

    # variable present - get definition statement
    #   - regex explanation
    #       ^   => beginning of line
    #       \s* => 0+ whitespace characters (used to bound variable and equal sign)
    #       \S+ => 1+ NON-whitespace characters (should be the value)
    definition_statement=$( grep -oE "^\s*${var}\s*=\s*\S+" $namelist_file )

    # trim off "var ="
    var_value=${definition_statement##*=}

    # trim off any remaining leading/trailing whitespace
    var_value=$( trim_whitespace $var_value )

    # remove trailing comma if present and send output
    var_value=$( echo $var_value | sed 's/,$//g' )
    echo $var_value 
}

function mod_nl(){
    # Modify a text file containing namelists
    # Each variable definition in this input namelist file must be on a line by itself
    # and be the of the form var =.* which will be replaced with var = value
    #
    # Usage: mod_nl namelist_file_in var1 [var2 ...]
    #   namelist_file_in  ...is an existing text file containing the namelists
    #   var1 var2 ...     ...names of variables to be changed
    #
    set -e
    local namelist_file
    local sed_script
    local var
    local val
    local ischar
    local variable_substitution_command
    local tmp_string
    is_defined $1 || bail "mod_nl requires a namelist file as the first arg"
    is_defined $1 || bail "mod_nl requires at least 1 variable name on the command line"
    namelist_file=$1
    is_file $namelist_file || bail "mod_nl input namelist file --> $namelist_file <-- is missing"
    shift

    # Create a backup copy of the input namelist file, overwriting any existing backup
    cp -f $namelist_file ${namelist_file}.bak

    # Write a sed script to make the requested substitutions
    sed_script=mod_nl_cmd
    rm -f $sed_script
    touch $sed_script
    for var in $*; do
      ischar=0
      # Any variable name beginning with _char_ is assumed to contain a string value
      # The leading _char_ prefix is removed from this name before further processing
      if [ x$(echo $var|sed -n '/^ *_char_/p') != x ]; then
        ischar=1
        var=$(echo $var|sed 's/^_char_//')
      fi
      eval val=\$$var
      if [ -z "$val" ]; then
        # If this variable is not defined then issue a warning and continue
        echo "mod_nl: $var is not defined"
        continue
      fi
      if [ $ischar -eq 1 ]; then
        # This is a character variable that needs to be quoted
        variable_substitution_command='s/^ *'$var' *=.*/'"  ${var} = \"$val\""'/'
      else
        variable_substitution_command='s/^ *'$var' *=.*/'"  ${var} = $val"'/'
      fi
      # Add this command to the file
      # Note, the quotes are required to preserve white space
      echo "$variable_substitution_command" >> $sed_script

      tmp_string=$(grep '^ *'$var' *=' $namelist_file | sed 's/ *//g')
      if [ "x$tmp_string" = "x" ]; then
        # This variable does not appear in the namelist file
        # Insert a line with a definition for var that will get modified below
        sed '1 a\
    '$var' = 0' $namelist_file > add_nl_var_$$
        mv add_nl_var_$$ $namelist_file
        echo "${namelist_file}: ADDED $var = $val"
      else
        # Echo the changes to stdout (useful for debugging runs)
        echo "${namelist_file}: RESET $var = $val"
      fi
    done

    # Make the substitutions in situ, overwriting the input file
    sed -f $sed_script $namelist_file > mod_nl_tmp_$$
    mv mod_nl_tmp_$$ $namelist_file
    rm $sed_script
    rm ${namelist_file}.bak
}

function days_in_month() {
   set -e
   # Determine the number of days in a given month (assuming 365 day year)
   local days_this_month
   is_defined "$1" || bail "--> days_in_month <-- requires an integer as the first arg"
   case $1 in
     1|01) days_this_month=31 ;;
     2|02) days_this_month=28 ;;
     3|03) days_this_month=31 ;;
     4|04) days_this_month=30 ;;
     5|05) days_this_month=31 ;;
     6|06) days_this_month=30 ;;
     7|07) days_this_month=31 ;;
     8|08) days_this_month=31 ;;
     9|09) days_this_month=30 ;;
       10) days_this_month=31 ;;
       11) days_this_month=30 ;;
       12) days_this_month=31 ;;
        *) bail "month = $1 is out of range" ;;
   esac
   echo $days_this_month
}

function update_nemo_counters(){
    set -e
    # Update nemo counters for the given start/stop dates, where the start/dates are 
    #   input in YYYY-MM-DD
    #
    #   Assumptions: 
    #       - time-steps divide evenly into a day
    #       - for the given days, start date refers to the beginning of the day, while stop date refers to the end of the day

    # values that get updated
    local nn_no         # job number
    local nn_it000      # starting step
    local nn_itend      # Final end step counter
    local nn_stock      # frequency of restart creation
    local nn_write      # frequency of output file creation
    local nn_date0      # date of initial calendar date yyyymmdd

    # input params
    local start_date
    local stop_date
    local ref_date
    local nemo_timestep 
    local namelist_file
    local job_no
    local restart_freq

    # worker vars
    local var val arg
    local timesteps_between_start_stop
    local timesteps_between_ref_start
    local start_year start_month
    local remainder
    local ref_stop_date
    local initial_timestep_counter
    for arg in "$@"; do
        case $arg in
            *=*)
                var=$(echo $arg | awk -F\= '{printf "%s",$1}')
                val=$(echo $arg | awk -F\= '{printf "%s",$2}')
                case $var in
                         start_date) start_date="$val";;        # YYYY-MM-DD
                          stop_date) stop_date="$val";;         # YYYY-MM-DD
                      nemo_timestep) nemo_timestep="$val" ;;    # seconds
                      namelist_file) namelist_file="$val" ;;    # absolute path to namelist file
                           ref_date) ref_date="$val";;          # optional - make step counters relative to this date
                             job_no) job_no="$val";;            # optional - job number
                       restart_freq) restart_freq="$val";;      # optional - frequency of restarts (iterations)
                        output_freq) output_freq="$val";;       # optional - frequency of output files (iterations)
                                  *) bail "Invalid command line arg --> $arg <--" ;;
                esac ;;
              *) bail "Invalid command line arg --> $arg <--" ;;
        esac
    done
    # check required vars
    is_defined $start_date      || bail "start_date variable is empty" 
    is_defined $stop_date       || bail "stop_date variable is empty" 
    is_defined $nemo_timestep   || bail "nemo_timestep variable is empty!"
    is_defined $namelist_file   || bail "namelist_file variable is empty!"

    # set optional vars
    ! is_defined $ref_date      && ref_date=$start_date                     # counters will be set from the very beginning of the calendar
    ! is_defined $job_no        && job_no=1
    ! is_defined $restart_freq  && restart_freq="set_to_total_step_count"   # make restart be output at final step count
    ! is_defined $output_freq   && output_freq="set_to_total_step_count"    # output at final step count

    if ! [[ $nemo_timestep =~ ^[1-9]+[0-9]*$ ]]; then
        bail "nemo_timestep must be an integer with no leading zeros (in seconds)!"
    fi

    # make sure timestep divides cleanly into 1 day
    remainder=$(( (3600*24) % nemo_timestep ))
    if (( remainder != 0 )); then
        bail "the given timestep must divide into 24 hours cleanly!"
    fi

    # determine days between dates
    timesteps_between_start_stop=$( calc_steps_between $start_date $stop_date $nemo_timestep )

    # determine the reference step number (initial step counter)
    if [[ $ref_date == $start_date ]]; then
        timesteps_between_ref_start=0
    else
        # increment start_date by negative one to get a stop date that can be used to determing the 
        #   reference counter (this is necessary as stop dates are assumed to include the final day)
        ref_stop_date=$( increment_day $start_date -1 )
        timesteps_between_ref_start=$( calc_steps_between $ref_date $ref_stop_date $nemo_timestep )
    fi
    initial_timestep_counter=$(( 1 + timesteps_between_ref_start ))

    # set nemo specific vars
    nn_no=$job_no
    nn_it000=$initial_timestep_counter
    nn_itend=$(( initial_timestep_counter + timesteps_between_start_stop - 1 )) # minus 1 to account for inclusion of first step
    if [[ $restart_freq == "set_to_total_step_count" ]]; then
        nn_stock=$timesteps_between_start_stop
    else
        nn_stock=$restart_freq
    fi
    if [[ $output_freq == "set_to_total_step_count" ]]; then
        nn_write=$timesteps_between_start_stop
    else
        nn_write=$output_freq
    fi
    #-- get nemo start date by converting YYYY-MM-DD to YYYYMMDD
    nn_date0=$(echo $start_date | sed "s/-//g")
     
    # make namelist mods 
    mod_nl $namelist_file nn_no nn_it000 nn_itend nn_stock nn_write nn_date0
}

function update_coupler_counters(){
    set -e
    # values that get updated in namelist
    local env_run_start_year
    local env_run_start_month
    local env_run_stop_year
    local env_run_stop_month

    # Note: these _also_ get updated in the namelist, but we should
    #   evaluate the need for them - they are redundant combinations of 
    #   env_runid, env_start_* and env_stop_*
    local env_model 
    local env_start

    # input params
    local start_date
    local stop_date
    local namelist_file
    local runid

    # worker vars
    local start_date_array stop_date_array
    local incremented_start_date
    for arg in "$@"; do
        case $arg in
            *=*)
                var=$(echo $arg | awk -F\= '{printf "%s",$1}')
                val=$(echo $arg | awk -F\= '{printf "%s",$2}')
                case $var in
                         start_date) start_date="$val"      ;; # YYYY-MM-DD
                          stop_date) stop_date="$val"       ;; # YYYY-MM-DD
                      namelist_file) namelist_file="$val"   ;; # absolute path to namelist file
                              runid) runid="$val"           ;; # runid string
                                  *) bail "Invalid command line arg --> $arg <--" ;;
                esac ;;
              *) bail "Invalid command line arg --> $arg <--" ;;
        esac
    done
    is_defined $start_date      || bail "start_date is empty" 
    is_defined $stop_date       || bail "stop_date is empty" 
    is_defined $namelist_file   || bail "namelist_file is empty!"
    is_defined $runid           || bail "runid is empty!"

    # set start/stop date values
    start_date_array=( ${start_date//-/ } )
    stop_date_array=( ${stop_date//-/ } )
    env_run_start_year=${start_date_array[0]}
    env_run_start_month=${start_date_array[1]}
    env_run_stop_year=${stop_date_array[0]}
    env_run_stop_month=${stop_date_array[1]}

    # set additional legacy variables
    #   - need to increment start_date by a day to get 'env_start' which follows the
    #     restart naming convention where, for example, 5549_m12 means starting from 5550-01-01
    incremented_start_date=$( increment_day $start_date -1 )
    incremented_start_date_array=( ${incremented_start_date//-/ } )
    incremented_start_year=${incremented_start_date_array[0]}
    incremented_start_month=${incremented_start_date_array[1]}
    env_model="mc_${runid}_${env_run_start_year}_m${env_run_start_month}_"
    env_start="mc_${runid}_${incremented_start_year}_m${incremented_start_month}_"

    # make changes
    mod_nl $namelist_file env_run_start_year env_run_start_month env_run_stop_year env_run_stop_month _char_env_model _char_env_start
}

function update_agcm_counters(){
    set -e
    # Update agcm counters for the given start/stop dates, where the start/dates are 
    #   input in YYYY-MM-DD format.
    #
    #   Assumptions: 
    #       - time-steps divide evenly into a day
    #       - for the given dates, that the stop date refers to the END of the day, while
    #         the start day refers to the start date

    # values that get updated
    local kfinal
    local kstart
    local iyear

    # input params
    local start_date
    local stop_date
    local ref_date
    local agcm_timestep 
    local namelist_file

    # worker vars
    local var val arg
    local total_days_between_start_stop
    local timesteps_between_start_stop
    local timesteps_between_ref_start
    local remainder
    local ref_stop_date
    local initial_timestep_counter
    for arg in "$@"; do
        case $arg in
            *=*)
                var=$(echo $arg | awk -F\= '{printf "%s",$1}')
                val=$(echo $arg | awk -F\= '{printf "%s",$2}')
                case $var in
                         start_date) start_date="$val";;        # YYYY-MM-DD
                          stop_date) stop_date="$val";;         # YYYY-MM-DD
                      agcm_timestep) agcm_timestep="$val" ;;    # seconds
                      namelist_file) namelist_file="$val" ;;    # absolute path to namelist file
                           ref_date) ref_date="$val";;          # optional - make step counters relative to this date
                                  *) bail "Invalid command line arg --> $arg <--" ;;
                esac ;;
              *) bail "Invalid command line arg --> $arg <--" ;;
        esac
    done
    # check required vars
    is_defined $start_date      || bail "start_date is empty" 
    is_defined $stop_date       || bail "stop_date is empty" 
    is_defined $agcm_timestep   || bail "agcm_timestep is empty!"
    is_defined $namelist_file   || bail "namelist_file is empty!"

    # set optional vars
    if ! is_defined $ref_date; then
        ref_date="0001-01-01" # counters will be set from the very beginning of the calendar
    fi

    # make sure the timestep is an integer
    if ! [[ $agcm_timestep =~ ^[1-9]+[0-9]*$ ]]; then
        bail "agcm_timestep must be an integer with no leading zeros (in seconds)!"
    fi

    # make sure timestep divides cleanly into 1 day
    remainder=$(( (3600*24) % agcm_timestep ))
    if (( remainder != 0 )); then
        bail "the given timestep must divide into 24 hours cleanly!"
    fi

    # determine days between dates
    timesteps_between_start_stop=$( calc_steps_between $start_date $stop_date $agcm_timestep )

    # determine the reference step number (initial step counter)
    if [[ $ref_date == $start_date ]]; then
        timesteps_between_ref_start=0
    else
        # increment start_date by negative one to get a stop date that can be used to determing the 
        #   reference counter (this is necessary as stop dates are assumed to include the final day)
        ref_stop_date=$( increment_day $start_date -1 )
        timesteps_between_ref_start=$( calc_steps_between $ref_date $ref_stop_date $agcm_timestep )
    fi
    initial_timestep_counter=$(( 1 + timesteps_between_ref_start ))

    # set agcm specific vars
    kstart=$timesteps_between_ref_start                                         # represents final timestep from the previous chunk/run
    kfinal=$(( initial_timestep_counter + timesteps_between_start_stop - 1))    # final timestep - minus 1 to account for inclusion of first step
    iyear=${start_date%%-*}

    # make namelist mods
    mod_nl ${namelist_file} kfinal kstart iyear
}

function increment_day(){
    set -e
    # take in a given date string in YYYY-MM-DD format and increment by the given amount of days
    local date_string
    local day_increment
    local date_array 
    local initial_year initial_month initial_day initial_day_of_year
    local new_day_of_year new_month_day new_year
    local year_increment
    date_string=$1
    day_increment=$2

    # make sure arguments were provided
    if ! is_defined $date_string || ! is_defined $day_increment; then
        bail "requires an input date string and day increment argument!"
    fi

    # make sure the right format of args were provided (YYYY-MM-DD for date_string and a negative/positive integer for 
    #   day_increment (where the first number can't be a 0)
    if ! [[ $date_string =~ ^[0-9]{4}-[0-9]{2}-[0-9]{2}$ ]] || ! [[ $day_increment =~ ^[-+]?[1-9]+[0-9]?$ ]]; then
        bail "the given date must be given in the following format YYYY-MM-DD and the day increment must be an integer without leading zeros!"    
    fi

    # split date string on '-'
    date_array=( ${date_string//-/ } )
    initial_year=$( strip_leading_zeros ${date_array[0]} )
    initial_month=$( strip_leading_zeros ${date_array[1]} )
    initial_day=$( strip_leading_zeros ${date_array[2]} )
    
    # convert initial month/day to day of year
    initial_day_of_year=$( conv_month_day_to_day_of_year $initial_month $initial_day )

    # increment day
    new_day_of_year=$(( initial_day_of_year + day_increment ))

    # check to see if we need to increment the year
    if (( new_day_of_year < 1 )) || (( new_day_of_year > 365 )); then

        if (( new_day_of_year < 1 )); then
            # went back a year - determine how many
            year_increment=$(( new_day_of_year/365 - 1 )) 
        elif (( new_day_of_year > 365 )); then
            # went forward a year - determine how many
            year_increment=$(( new_day_of_year/365 )) 
        fi

        # increment and update day
        new_year=$(( initial_year + year_increment ))
        new_day_of_year=$(( new_day_of_year - year_increment*365 ))
    else
        new_year=$initial_year
    fi

    # convert back to MM-DD string
    new_month_day=$( conv_day_of_year_to_month_day $new_day_of_year )

    # return new date string, noting that new_month_day is already in MM-DD format
    printf "%04d-%s" $new_year $new_month_day
}

function calc_steps_between(){
    set -e
    # given 
    #   $1 -> start_date
    #   $2 -> end_date
    #   $3 -> timestep (seconds)
    # determine the number of time steps between dates, where the dates are given
    # given in YYYY-MM-DD format, and the start_date refers to the beginning of the day
    #
    # Note: assumes that times steps divide cleanly into a day
    local start_date
    local end_date
    local timestep
    local days_between_dates timesteps_per_day steps_between_dates remainder
    start_date=$1
    end_date=$2
    timestep=$3

    # make sure arguments were provided
    if ! is_defined $start_date || ! is_defined $end_date || ! is_defined $timestep; then
        bail "requires three arguments START_DATE END_DATE TIMESTEP"
    fi

    # make sure timestep divides cleanly into 1 day
    remainder=$(( (3600*24) % timestep ))
    if (( remainder != 0 )); then
        bail "the given timestep must divide into 24 hours cleanly!"
    fi

    # determine timesteps per day
    timesteps_per_day=$(( 3600*24 / timestep ))

    # get days between
    days_between_dates=$( days_between $start_date $end_date )

    # determine total timesteps between dates and return
    steps_between_dates=$(( days_between_dates * timesteps_per_day ))
    echo $steps_between_dates 
}

function conv_month_day_to_day_of_year(){
    set -e
    # take in a month/day and return the day of year
    #   i.e. Feb 1st would be the 32nd day of the year.
    local month
    local day
    local dayofyear
    local m
    month=$1
    day=$2
    if ! is_defined $month || ! is_defined $day; then
        bail "requires both month and day!"
    fi
    if (( month > 12 || month < 1 )) || (( day > $( days_in_month $month ) || day < 1 )); then
        bail "invalid month/day combo! -> $month : $day"
    fi

    # determine day of year by counting for each month
    dayofyear=$day 
    if ! (( month == 1 )); then
        # need to loop through all months prior to the given one to get contribution
        for m in $(seq 1 $(( month - 1 ))); do
            dayofyear=$(( dayofyear + $( days_in_month $m ) ))
        done
    fi
    echo $dayofyear
}

function conv_day_of_year_to_month_day(){
    set -e
    # take in a day of year and convert to a MM-DD string
    #   i.e. the day 32 would be Feb 1st
    local input_day_of_year
    local month day
    local month_boundary_day_of_year
    input_day_of_year=$1

    # check args
    if ! is_defined $input_day_of_year ; then
        bail "needs day of year given as an argument!"
    fi
    if ! [[ $input_day_of_year =~ ^[0-9]+$ ]]; then
        bail "day of year must be given as an integer!"
    fi

    # determine what month we are in and what the day of year is up to the first of that month
    if   (( input_day_of_year >= 1   && input_day_of_year <= 31  )); then
        # january
        month_boundary_day_of_year=1
        month=1
    elif (( input_day_of_year >= 32  && input_day_of_year <= 59  )); then
        # feb
        month_boundary_day_of_year=32
        month=2
    elif (( input_day_of_year >= 60  && input_day_of_year <= 90  )); then
        # march
        month_boundary_day_of_year=60
        month=3
    elif (( input_day_of_year >= 91  && input_day_of_year <= 120 )); then
        # april
        month_boundary_day_of_year=91
        month=4
    elif (( input_day_of_year >= 121 && input_day_of_year <= 151 )); then
        # may
        month_boundary_day_of_year=121
        month=5
    elif (( input_day_of_year >= 152 && input_day_of_year <= 181 )); then
        # june
        month_boundary_day_of_year=152
        month=6
    elif (( input_day_of_year >= 182 && input_day_of_year <= 212 )); then
        # july
        month_boundary_day_of_year=182
        month=7
    elif (( input_day_of_year >= 213 && input_day_of_year <= 243 )); then
        # august
        month_boundary_day_of_year=213
        month=8
    elif (( input_day_of_year >= 244 && input_day_of_year <= 273 )); then
        # september
        month_boundary_day_of_year=244
        month=9
    elif (( input_day_of_year >= 274 && input_day_of_year <= 304 )); then
        # october
        month_boundary_day_of_year=274
        month=10
    elif (( input_day_of_year >= 305 && input_day_of_year <= 334 )); then
        # november
        month_boundary_day_of_year=305
        month=11
    elif (( input_day_of_year >= 335 && input_day_of_year <= 365 )); then
        # december
        month_boundary_day_of_year=335
        month=12
    else
        bail "invalid julian day! Must be 1-365"
    fi

    # determine the day (1 added to account for the first day of the month always being the 1st)
    day=$(( input_day_of_year - month_boundary_day_of_year + 1 ))
    printf "%02d-%02d" $month $day
}

function days_between(){
    set -e
    # take in two dates with YYYY-MM-DD format and calculate number of days between them
    #   assuming:
    #       - start dates refer to the BEGINNING of the day
    #       - stop dates refer to the END of the day
    local start_date stop_date
    local start_date_array stop_date_array
    local start_year start_month start_day stop_year stop_month stop_day
    local start_day_of_year stop_day_of_year
    local total_days
    start_date=$1
    stop_date=$2

    # make sure we got arguments
    if ! is_defined $start_date || ! is_defined $stop_date; then
        bail "requires both a start_date (first positional arg) and stop_date (second positional arg)!"
    fi
    # make sure we have the right format -> YYYY-MM-DD
    if ! [[ $start_date =~ ^[0-9]{4}-[0-9]{2}-[0-9]{2}$ ]] || ! [[ $stop_date =~ ^[0-9]{4}-[0-9]{2}-[0-9]{2}$ ]]; then
        bail "the given dates must be given in the following format YYYY-MM-DD"    
    fi

    # parse dates by splitting on '-' and pulling necessary fields
    #   removing leading zeros
    start_date_array=( ${start_date//-/ } )
    stop_date_array=( ${stop_date//-/ } )
    start_year=$( strip_leading_zeros ${start_date_array[0]} )
    start_month=$( strip_leading_zeros ${start_date_array[1]} )
    start_day=$( strip_leading_zeros ${start_date_array[2]} )
    stop_year=$( strip_leading_zeros ${stop_date_array[0]} )
    stop_month=$( strip_leading_zeros ${stop_date_array[1]} )
    stop_day=$( strip_leading_zeros ${stop_date_array[2]} )

    # make sure stop_date > start_date
    if (( start_year > stop_year )) || 
       (( start_year == stop_year && start_month > stop_month )) || 
       (( start_year == stop_year && start_month == stop_month && start_day > stop_day )) ; then
        bail "the given start_date, $start_date, is after the given stop_date, $stop_date!"
    fi

    # bail if any non-valid days given
    if (( start_month > 12 || stop_month > 12 )) ||         # months > 12
       (( start_month < 1  || stop_month < 1  )) ||         # months < 1
       (( start_day   < 1  || stop_day   < 1  )) ||         # days   < 1
       (( start_day > $( days_in_month $start_month ) )) || # days outside valid days for given month
       (( stop_day  > $( days_in_month $stop_month  ) )); then
        bail "invalid date!"
    fi

    # convert the given month/day combos into day of year (i.e. December 30 = day 364 ) 
    start_day_of_year=$( conv_month_day_to_day_of_year $start_month $start_day )
    stop_day_of_year=$( conv_month_day_to_day_of_year $stop_month $stop_day )

    # calculate total days and output (1 is added because we include the stop day)
    total_days=$(( ( stop_year - start_year )*365 + ( stop_day_of_year - start_day_of_year ) + 1 ))
    echo $total_days
}

function calc_chunk_stop_date(){
    set -e
    # take in a given start date (YYYY-MM-DD) and add the given chunk time and 
    #   create a stop day string of the same format.
    #
    #   TEMPORARY ASSUMPTIONS:
    #       - start at the beginning of month
    #       - end at the end of month
    # 
    #   In the future, it would be great if we can remove the above assumptions
    local start_date
    local chunk_size
    local start_year start_month start_day start_date_array
    local end_year end_month end_day
    local full_years extra_months
    start_date=$1
    chunk_size=$2
    
    # consistency checks
    if ! is_defined $start_date || ! is_defined $chunk_size; then
        bail "needs both a start date and chunk size"
    fi
    if ! [[ $start_date =~ ^[0-9]{4}-[0-9]{2}-[0-9]{2}$ ]]; then
        bail "the given start_date must be given YYYY-MM-DD format"    
    fi

    # currently we assume that chunk_size is in months, make sure its a integer
    if ! [[ $chunk_size =~ ^[0-9]+$ ]]; then
        bail "the given chunk size must be an integer representing the number of months!"
    fi

    # split date and remove leading zeros
    start_date_array=( ${start_date//-/ } )
    start_year=$( strip_leading_zeros ${start_date_array[0]} )
    start_month=$( strip_leading_zeros ${start_date_array[1]} )
    start_day=$( strip_leading_zeros ${start_date_array[2]} )

    # make sure date is fine
    if (( start_month > 12 )) || (( start_month < 1 )) ||
       (( start_day < 1 || start_day > $( days_in_month $start_month ) )); then
        bail "invalid start date: $start_date"
    fi

    # currently our infrastructure only allows first of the month start dates, so put in 
    #   temporary bail condition for that until we adjust
    if (( start_day != 1 )); then
        bail "arbitrary start dates are currently unsupported and run must start at beginning of month!"
    fi

    # determine chunk stop time 
    #   - minus 1 is used to account for one month just incrementing from beginning of month to end of month
    full_years=$((   ( chunk_size - 1 ) / 12 )) 
    extra_months=$(( ( chunk_size - 1 ) % 12 ))
    end_year=$(( start_year + full_years ))
    end_month=$(( start_month + extra_months ))
    if (( end_month > 12 )); then
        # need to roll over to next year
        end_year=$(( end_year + 1 ))
        end_month=$(( end_month % 12 ))
    fi
    end_day=$( days_in_month $end_month )

    # return
    printf "%04d-%02d-%02d" $end_year $end_month $end_day
}

function calc_chunk_start_date(){
    set -e 
    # take in a given stop date (YYYY-MM-DD) and and chunk time (in months) and
    #   back calculate the start date in the same format.
    #
    #   Currently assumes:
    #       - start dates are the beginning of the month
    #       - end dates are at the end of the month
    #       - chunk size is given in months
    local stop_date
    local chunk_size
    local stop_year stop_month stop_day stop_date_array
    local start_year start_month
    local full_years
    local extra_months
    stop_date=$1
    chunk_size=$2

    # consistency checks
    if ! is_defined $stop_date || ! is_defined $chunk_size; then
        bail "needs both a start date and chunk size"
    fi
    if ! [[ $stop_date =~ ^[0-9]{4}-[0-9]{2}-[0-9]{2}$ ]]; then
        bail "the given start_date must be given YYYY-MM-DD format"    
    fi

    # currently we assume that chunk_size is in months, make sure its a integer
    if ! [[ $chunk_size =~ ^[0-9]+$ ]]; then
        bail "the given chunk size must be an integer representing the number of months!"
    fi

    # split date and remove leading zeros
    stop_date_array=( ${stop_date//-/ } )
    stop_year=$( strip_leading_zeros ${stop_date_array[0]} )
    stop_month=$( strip_leading_zeros ${stop_date_array[1]} )
    stop_day=$( strip_leading_zeros ${stop_date_array[2]} )

    # make sure date is fine
    if (( stop_month > 12 )) || (( stop_month < 1 )) ||
       (( stop_day < 1 || stop_day > $( days_in_month $stop_month ) )); then
        bail "invalid stop date: $stop_date"
    fi

    # currently our infrastructure only allows end of month stop dates, so put in 
    #   temporary bail condition for that until we adjust
    if (( stop_day != $( days_in_month $stop_month ) )); then
        bail "arbitrary stop dates are currently unsupported and run must stop at the end of the month!"
    fi

    # determine start date
    if (( stop_month >= chunk_size )); then
        start_month=$(( stop_month - chunk_size + 1 ))
        start_year=$stop_year
    else
        full_years=$(( chunk_size / 12 ))
        extra_months=$(( chunk_size % 12 ))
        start_year=$(( stop_year - full_years ))
        start_month=$(( stop_month - extra_months + 1 ))

        if (( start_month <= 0 )); then
            # need to roll back a year
            start_year=$(( start_year - 1 ))
            start_month=$(( 12 + start_month ))
        fi
    fi
    
    # return    
    printf "%04d-%02d-%02d" $start_year $start_month 1 # always start and beginning of the month
}

function ToF(){
    # purpose: potentially reset the value of the given variable to 0 (false) or 1 (true)
    #           where the original value could be 'on'/'off', or 'yes'/'no'
    #   NOTE: we should just be consistent with our booleans ....
    local var
    local var_value
    var=$1
    is_defined $var || bail "ToF: requires a variable name as an input!"

    # get the variable value and trim whitespace
    eval var_value=\$$1
    var_value=$(trim_whitespace $var_value)

    # potentially reassign the value to 0/1
    case $var_value in
        yes|on) var_value=1 ;;
        no|off) var_value=0 ;;
             *) : ;;            # leave the value the way it is
    esac

    # set the updated value
    eval $var=$var_value
}
