#! /bin/sh
# set -x
# This "runid_clone_setup_hare.sh" script is to be put,
# revised as needed, and invoked from the same 
# subdirectory where "runid" base runid resides.

Run_Base_dir=`pwd`

# =========== Adjust this section as necessary:
# RUNID
runid='gcm18_02xxx' ; export runid
# target host
hst='hare' ; export hst
RUNPATH_ROOT=${RUNPATH_ROOT:-"/space/hall1/work/eccc/crd/ccrn"} ; export RUNPATH_ROOT
RUNPATH="$RUNPATH_ROOT/users/`whoami`" ; export RUNPATH


## # Specify repos and versions to checkout. Could be command line options.
# CanESM "git" version
CanESM_REPO=git@gitlab.science.gc.ca:CanESM/CanESM5.git
CanESM_VER=${CanESM_VER:-'CanESM5_XC40_conversion'} ; export CanESM_VER
CanESM_VER=${CanESM_VER:-'b824b27c'} ; export CanESM_VER

# ============ What follows should be OK as is:

# Ensure "$RUNPATH_ROOT" valid setting.
if [ -n "$RUNPATH_ROOT" -a -d "$RUNPATH_ROOT/." ] ; then
 :
else
 echo "Problem: Invalid $RUNPATH_ROOT subdirectory !"
 exit 1
fi
# Ensure "$RUNPATH" valid setting.
if [ -n "$RUNPATH" -a -d "$RUNPATH/." ] ; then
 :
else
 echo "Problem: Invalid RUNPATH=$RUNPATH !"
 exit 1
fi


# File to use under "$RUNPATH/PRODUCTION/${runid}/generic"
# in setting up "env_setup_file" file under the same subdirectory
# where the latter will be executed to setup the run environment.

generic_env_setup_file="${hst}_linux_setup_profile" ; export generic_env_setup_file

RUNID_ROOT="$RUNPATH/PRODUCTION/${runid}" ; export RUNID_ROOT
CCRNSRC="$RUNID_ROOT" ; export CCRNSRC

if [ -d "$RUNID_ROOT/." ] ; then
 echo "Sorry; $RUNID_ROOT already exists!"
 exit 1
fi

# Create $RUNID_ROOT and other subdirectories underneath it, and chekout 
# code for CCCma_tools,CanAM,CanDiag, ...
# from "git" repository into it.

echo "=== Proceed in creating subdirectories and get needed code "
echo "    from git repositories: Start at `date`" 
#

[ ! -d "$RUNID_ROOT/." ]  && mkdir -p -m 1755 $RUNID_ROOT || :
if [ -d "$RUNID_ROOT/." ] ; then
 cd $RUNID_ROOT
 HMTMP="$RUNID_ROOT/tmp" ; export HMTMP
 [ ! -d "$HMTMP/." ] && mkdir -m 1755 $HMTMP 
 OUTPUTQ="$RUNID_ROOT/.queue" ; export OUTPUTQ
 [ ! -d "$OUTPUTQ/." ] && mkdir -m 1755 $OUTPUTQ
 [ ! -d "$OUTPUTQ/.data/." ] && mkdir -m 1755 $OUTPUTQ/.data 
 [ ! -d "$OUTPUTQ/.crawork/." ] && mkdir -m 1755 $OUTPUTQ/.crawork 
 RTEXTBLS="$RUNID_ROOT/executables" ; export RTEXTBLS
 [ ! -d "$RTEXTBLS/." ] && mkdir -m 1755 $RTEXTBLS 
 SCRIPTS="$RUNID_ROOT/scripts" ; export SCRIPTS

 # CCRNOPT="$RUNID_ROOT" ; export CCRNOPT
 CCRNOPT="/home/scrd102" ; export CCRNOPT
 CCRNOPTBIN="$CCRNOPT/package/bin" ; export CCRNOPTBIN
 RUNID_BIN="$RUNID_ROOT/bin" ; export RUNID_BIN
 [ ! -d "$RUNID_BIN/." ] && mkdir -m 1755 $RUNID_BIN 
 RUNID_LIB="$RUNID_ROOT/lib" ; export RUNID_LIB
 [ ! -d "$RUNID_LIB/." ] && mkdir -m 1755 $RUNID_LIB 
 SITE_ID='DrvlSC' ; export SITE_ID
 
else
 echo "Problem: Invalid $RUNID_ROOT subdirectory !"
 exit 1
fi
 
cd $RUNID_ROOT

# Checkout targeted git repositories 

# clone the tools with recursive to get submodules
git clone $CanESM_REPO canesm
cd canesm
git checkout $CanESM_VER
git submodule init
git config -l
git submodule update --recursive
cd $RUNID_ROOT/canesm/CCCma_tools
git submodule init
git config -l
git submodule update --recursive

printf "\n\n *** \n  Successfully checked out CanESM5 ${CanESM_VER} into TMPSRC. \n\n"

cd $RUNID_ROOT
ln -s canesm/CCCma_tools/bin_xc_linux64 bin_xc_linux64
ln -s canesm/CCCma_tools/cccjob_dir cccjob_dir
ln -s canesm/CCCma_tools/cccmake cccmake
ln -s canesm/CCCma_tools/generic generic
ln -s canesm/CCCma_tools/scripts scripts
ln -s scripts scripts_linux
ln -s canesm/CCCma_tools/tools tools

ln -s $RUNID_ROOT/canesm/CanCPL/bin/gitrip $RUNID_BIN/gitrip

mkdir -m 1755 $RUNID_ROOT/source
cd $RUNID_ROOT/source
ln -s ../canesm/CanDIAG/diag4 diag4
ln -s ../canesm/CanDIAG/lspgm lspgm
ln -s ../canesm/CanDIAG/lssub lssub
ln -s ../canesm/CanAM/lsmod lsmod
cd $RUNID_ROOT
ln -s source source_linux
ln -s source source_xc_linux64
(cd $RUNID_ROOT/canesm/CanDIAG/lssub/model/agcm && ln -s $RUNID_ROOT/canesm/CanAM/lssub/model/agcm/gcm18 gcm18  || : )

#
# [ ! -d "./bin_xc_linux64/." ] && mkdir -m 1755 bin_xc_linux64 || :
hash -r
DFLTPATH='/opt/pbs/13.0.401.160392/bin:/opt/cray/rca/1.0.0-2.0502.60530.1.62.ari/bin:/opt/cray/alps/5.2.4-2.0502.9774.31.11.ari/sbin:/opt/cray/dvs/2.5_0.9.0-1.0502.2188.1.116.ari/bin:/opt/cray/xpmem/0.1-2.0502.64982.5.3.ari/bin:/opt/cray/pmi/5.0.10-1.0000.11050.0.0.ari/bin:/opt/cray/ugni/6.0-1.0502.10863.8.29.ari/bin:/opt/cray/udreg/2.3.2-1.0502.10518.2.17.ari/bin:/opt/cray/craype/2.5.4/bin:/opt/cray/cce/8.4.6/cray-binutils/x86_64-unknown-linux-gnu/bin:/opt/cray/cce/8.4.6/craylibs/x86-64/bin:/opt/cray/cce/8.4.6/cftn/bin:/opt/cray/cce/8.4.6/CC/bin:/opt/cray/switch/1.0-1.0502.60522.1.61.ari/bin:/opt/cray/eslogin/eswrap/1.3.3-1.020200.1278.0/bin:/opt/modules/3.2.10.3/bin:/home/scrd103/bin:/usr/local/bin:/usr/bin:/bin:/usr/bin/X11:/usr/X11R6/bin:/usr/games:/usr/lib/mit/bin:/usr/lib/mit/sbin:/sbin:/usr/sbin:/usr/lib/qt3/bin:/opt/dell/srvadmin/bin:/opt/cray/bin' ; export DFLTPATH
# PATH=`$SCRIPTS/subproc/clnstrng "$RUNID_ROOT/bin_xc_linux64:$RUNID_BIN:$SCRIPTS/comm:$SCRIPTS/subproc:$SCRIPTS/sx:$RTEXTBLS:$RTEXTBLS/svpgm:$CCRNOPTBIN:$RTEXTBLS/lopgmhr:$DFLTPATH" ':'` ; export PATH
PATH="$RUNID_ROOT/bin_xc_linux64:$RUNID_BIN:$SCRIPTS/comm:$SCRIPTS/subproc:$SCRIPTS/sx:$RTEXTBLS:$RTEXTBLS/svpgm:$CCRNOPTBIN:$RTEXTBLS/lopgmhr:$DFLTPATH" ; export PATH
hash -r

cd $RUNID_ROOT
PATH=`$SCRIPTS/subproc/clnstrng "$PATH" ':'` ; export PATH
hash -r
if [ -s "$RUNID_ROOT/generic/env_setup_file" ] ; then
 echo "WARNING - File $RUNID_ROOT/generic/env_setup_file already exists and is expected to be valid"
 \ls -ld $RUNID_ROOT/generic/env_setup_file
 echo "You need to have it recreated if it should have different contents in it"
else
 if [ -n "$generic_env_setup_file" -a -s "$RUNID_ROOT/generic/$generic_env_setup_file" ] ; then
  echo "Attempt setting up generic/env_setup_file from $RUNID_ROOT/generic/$generic_env_setup_file "
  (\rm -f $RUNID_ROOT/generic/env_setup_file || : )
 echo " echo ' -- env_setup_file setup underway -- ' " > $RUNID_ROOT/generic/env_setup_file
 echo " RUNPATH=$RUNPATH ; export RUNPATH
 CanAM_VER=$CanAM_VER ; export CanAM_VER
 CanDIAG_VER=$CanDIAG_VER ; export CanDIAG_VER
 CCCma_TOOLS_VER=$CCCma_TOOLS_VER ; export CCCma_TOOLS_VER
 runid=$runid ; export runid " >> $RUNID_ROOT/generic/env_setup_file
 cat $RUNID_ROOT/generic/$generic_env_setup_file >> $RUNID_ROOT/generic/env_setup_file
 ls -ld $RUNID_ROOT/generic/$generic_env_setup_file $RUNID_ROOT/generic/env_setup_file
 else
  echo "Expected $RUNID_ROOT/generic/$generic_env_setup_file file is missing!"
  echo "Skipped setting up $RUNID_ROOT/generic/env_setup_file "
 fi
fi
if [ ! -s "$Run_Base_dir/rsubx" -a -s "$RUNID_ROOT/generic/env_setup_file" ] ; then
echo '#! /bin/sh
Rsub_dirctv="$*"' > $Run_Base_dir/rsubx
echo ". $RUNID_ROOT/generic/env_setup_file
hash -r " >> $Run_Base_dir/rsubx
echo 'exec rsub  $Rsub_dirctv' >> $Run_Base_dir/rsubx
chmod a+rx $Run_Base_dir/rsubx 
echo "Created $Run_Base_dir/rsubx interface script for job submission"
fi
set -x
pwd
\ls -al
set +x
echo "=== End of cloning at `date`" 
#!!!!!


if [ -s "$RUNPATH/PRODUCTION/${runid}/tools/compile_CanDIAG_${hst}" ] ; then
 cd $RUNPATH/PRODUCTION/${runid}/tools
 # Attempt creating needed program executables under "executables" subdirectory.
 echo $PATH
 time ./compile_CanDIAG_${hst}
 [ -s "$RUNPATH/PRODUCTION/${runid}/generic/env_setup_file" ] && echo "Setup file to target: $RUNPATH/PRODUCTION/${runid}/generic/env_setup_file" || :
else
 echo "Missing expected $RUNPATH/PRODUCTION/${runid}/tools/compile_CanDIAG_${hst} file!"
fi
