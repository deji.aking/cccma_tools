import pytest
import sys
import os

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from canesm import CanESMensemble


setup_folder = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'setup_files')


@pytest.mark.skip('no finished simulation to test setup with at the moment')
class TestExtend:

    def test_extend(self):
        ens = CanESMensemble.from_config_file(os.path.join(setup_folder, 'test_config_table_dates.yaml'))
        ens.extend_ensemble(10)


if __name__ == '__main__':
    TestExtend().test_extend()
