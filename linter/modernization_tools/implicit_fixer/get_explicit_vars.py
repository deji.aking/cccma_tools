#!/usr/bin/python3
import re
import sys

def main():
    log = sys.argv[1]
    lines = []
    with open(log, 'r') as f:
        lines = f.readlines()

    implicit_errors = []
    other_errors = []
    integer_values = ['i', 'j', 'k', 'l', 'm', 'n']
    filename = ''

    for i, line in enumerate(lines):
        if 'Error:' in line:
            #if filename == '':
            #    filename = re.match(r'\.\./\.\./([^:]*):.*', lines[i+1])[1]
            implicit_var = re.match(r'.*‘([^’]*)’ at \(\d\) has no IMPLICIT type.*', line)
            if implicit_var is None:
                other_errors.append(line)
            else:
                implicit_errors.append(implicit_var[1])
    
    implicit_errors = list(set(implicit_errors))
    implicit_errors.sort()

    for err in implicit_errors:
        if err[0] in integer_values:
            print(f'  integer :: {err}')
        else:
            print(f'  real :: {err}')

    for err in other_errors:
        sys.stderr.write(err[:-1])
    if len(other_errors) > 0:
        sys.stderr.write('')
        sys.stderr.write(filename)


if __name__ == '__main__':
    main()