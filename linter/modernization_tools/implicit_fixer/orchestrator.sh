#!/bin/bash

for file in "$@"; do
    impl=$(cat $file | grep "implicit none")
    [[ ! -z "$impl" ]] && continue
    filename=${file##*/}
    echo "Adding vars to $filename..."
    python3 make_implicit_none.py $file || exit 1
    gfortran temp.f90 > errors.txt 2>&1 # compiler specified here
    python3 get_explicit_vars.py errors.txt > vars.txt
    python3 add_explicit_vars.py $file
    rm -f vars.txt
    rm -f errors.txt
    rm -f temp.f90
done
