'''
Main code body for linting functionality.
'''

import sys
import re

def lint(filelist, rules):
    total_errors = []
    for file in filelist:
        with open(file, 'r') as f:
            lines = f.readlines()
        errors = []
        warnings = []

        # Check that explicit typing is used:
        #   1) Enforce implicit none
        #   2) No 'custom' implicit declarations
        if file not in rules['explicit_typing']['exceptions']:
            if rules['explicit_typing']['level'] == 'error':
                errors.extend(check_implicit_none(lines))
                errors.extend(check_implicit_typing(lines))
            elif rules['explicit_typing']['level'] == 'warning':
                warnings.extend(check_implicit_none(lines))
                warnings.extend(check_implicit_typing(lines))

        # Check all relevant variables have intent declarations
        if file not in rules['intent']['exceptions']:
            if rules['intent']['level'] == 'error':
                errors.extend(check_intent(lines))
            elif rules['intent']['level'] == 'warning':
                warnings.extend(check_intent(lines))

        # Locate all goto statements
        if file not in rules['goto']['exceptions']:
            if rules['goto']['level'] == 'error':
                errors.extend(check_goto(lines))
            elif rules['goto']['level'] == 'warning':
                warnings.extend(check_goto(lines))

        # Check all variables are declared in their own statements
        single_violations, sep_violations = check_declarations(lines)
        if file not in rules['single_var']['exceptions']:
            if rules['single_var']['level'] == 'error':
                errors.extend(single_violations)
            elif rules['single_var']['level'] == 'warning':
                warnings.extend(single_violations)
        if file not in rules['decl_separation']['exceptions']:
            if rules['decl_separation']['level'] == 'error':
                errors.extend(sep_violations)
            elif rules['decl_separation']['level'] == 'warning':
                warnings.extend(sep_violations)

        # Check all files are free-form
        if file not in rules['fixed_form']['exceptions']:
            if rules['fixed_form']['level'] == 'error':
                errors.extend(check_fixed_form(lines))
            elif rules['fixed_form']['level'] == 'warning':
                warnings.extend(check_fixed_form(lines))

        # Locate all hollerith variables
        if file not in rules['hollerith_var']['exceptions']:
            if rules['hollerith_var']['level'] == 'error':
                errors.extend(check_hollerith_var(lines))
            elif rules['hollerith_var']['level'] == 'warning':
                warnings.extend(check_hollerith_var(lines))

        errors.sort(key=lambda x: x[0])
        warnings.sort(key=lambda x: x[0])

        if len(warnings) > 0:
            print(f'Warning: {file}')
            for war in warnings:
                print(f'    {war[0]}: {war[1][:-1]}')
                print(f'        {war[2]}')
        if len(errors) > 0:
            print(f'ERROR: {file}')
            for err in errors:
                print(f'    {err[0]}: {err[1][:-1]}')
                print(f'        {err[2]}')
        total_errors.extend(errors)
    if len(total_errors) > 0:
        sys.exit(1)

def check_implicit_typing(lines):
    """Check to make sure that no variables are manually implicitly types

    :param lines: All the lines in the source code
    :type lines: list of strings
    :return: All Fortran programming constructs that use explicit typing
    :rtype: list of strings
    """

    structure_header = re.compile(r'^\s*(program|module|subroutine|function)\s+(\w+)', re.IGNORECASE)
    implicit_typing = re.compile(r'^\s*implicit\s*(real|integer|complex|character|logical)', re.IGNORECASE)

    violations = []
    for i, line in enumerate(lines):
        new_structure_detected = re.match(structure_header, line)
        if new_structure_detected:
            current_structure = new_structure_detected[2]
        if re.match(implicit_typing, line):
            violations.append((i+1, current_structure, 'implicit typing in subprogram'))
    return violations

def check_implicit_none(lines):
    """Check source code to ensure that explicit typing is used

    :param lines: All the lines in the source code
    :type lines: list of strings
    :return: All Fortran programming constructs that use explicit typing
    :rtype: list of strings
    """


    violations = []
    current_structure = ""
    structure_header = re.compile(r'^\s*(module|subroutine|function)\s+(\w+)', re.IGNORECASE)
    implicit_none = re.compile(r'^\s*implicit\s+none')
    module_identifier = re.compile(r'^\s*module\s+(\w+)', re.IGNORECASE)

    in_module = False

    for i, line in enumerate(lines):
        new_structure_detected = re.match(structure_header, line)
        is_module = re.match(module_identifier, line)
        in_module = in_module or is_module
        if new_structure_detected:
            # avoid flagging routines with different, cpp key specific, signatures
            if not current_structure == new_structure_detected[2]:
                current_structure = new_structure_detected[2]
                if is_module:
                    violations.append((i+1, current_structure, 'implicit none not present in module'))
                elif not in_module:
                    violations.append((i+1, current_structure, 'implicit none not present in program component'))

        if re.match(implicit_none, line) and len(violations)>0:
            violations.pop()
    return violations

def check_intent(lines):
    violations = []
    variables = []
    continuation = False
    for i, line in enumerate(lines):
        sub_line = re.match(r'^\s*(subroutine|function)\s+(\w+)\s*\(\s*([^\n&!\)]*)(\)?)(&?)', line, re.IGNORECASE)
        if sub_line:
            # if a new function/subroutine, initiate new variable list
            variables.clear()

            # extract variables from the 3rd group in the above regex and add first line variables if any
            newVars = [x.strip() for x in sub_line[3].split(',') if len(x.strip()) > 0]
            if len(newVars) > 0:
                variables.extend(newVars)

            # if this line has a continuation character, make it known for the next line
            if len(sub_line[5]) > 0:
                continuation = True
            continue

        if continuation:
            # pull the remaining variables in this routine/function signature
            sub_line = re.match(r'\s*([^!&\)]*)(\)?)(&?)', line, re.IGNORECASE)

            # variables will be contained in the first regex grouping
            newVars = [x.strip() for x in sub_line[1].split(',') if len(x.strip()) > 0]
            if len(newVars) > 0:
                variables.extend(newVars)

            # check for a closing ')' in the function signature
            if len(sub_line[2]) > 0:
                continuation = False
            continue

        # for variables in the call signature make sure they have intent
        var_line = re.match(r'^\s*(real|integer|logical|character|complex)[^:!&]*::\s*\b(\w+)\b', line, re.IGNORECASE)
        if var_line:
            if var_line[2] in variables:
                if re.match(r'^[^&!\n]*\bintent\b\(', line, re.IGNORECASE):
                    variables.remove(var_line[2])
                else:
                    violations.append((i+1, line, f'Variable {var_line[2]} has no intent specified'))
    return violations

def check_goto(lines):
    violations = []
    for i, line in enumerate(lines):
        if re.match(r'^[^!&]*\bgo\s*to\b', line, re.IGNORECASE):
            violations.append((i+1, line, 'goto statement detected'))

        if re.match(r'^\s*if\b[^!&\n]*\s*\d+\s*,\s*\d+\s*,\s*\d+[^&]*[\n!]', line, re.IGNORECASE):
            violations.append((i+1, line, 'arithmetic if detected'))
    return violations

def check_declarations(lines):
    i = 0
    single_violations = []
    sep_violations = []
    while i < len(lines):
        try:
            declaration_lines = []
            if lines[i] is None or len(lines[i]) == 1:
                i += 1
                continue
            if is_declaration(lines[i]) and not has_continuation(lines[i-1]):
                if not re.search(r'::', lines[i]):
                    sep_violations.append((i+1, lines[i], 'variable declarations must contain ::'))
                    i += 1
                    continue
                while has_continuation(lines[i]) or is_blank(lines[i]) or is_comment(lines[i]):
                    if has_continuation(lines[i]):
                        declaration_lines.append(lines[i])
                    i += 1
                declaration_lines.append(lines[i])
                declaration = ''.join([split_line_comment(l)[0] for l in declaration_lines])
                variables = re.match(r'^([^!&]*)::\s*(.*)$', declaration.replace('\n',''))
                if not variables:
                    print(declaration_lines)
                if len(split_variables(variables[2])) > 1:
                    single_violations.append((i+1, lines[i], 'multiple variables declared in one statement'))
            i += 1
        except:
            print(i)
            print(lines[i])
            sys.exit()
    return single_violations, sep_violations

def check_fixed_form(lines):
    for i, line in enumerate(lines):
        if re.match(r'^C\b', line, re.IGNORECASE):
            return [(i+1, line, 'fixed-form syntax detected')]
    return []

def check_hollerith_var(lines):
    violations = []
    for i, line in enumerate(lines):
        hol = re.search(r'\b(\d+H\w+)\b', line)
        if hol and not is_comment(line):
            violations.append((i+1, line, f'hollerith variable {hol[1]} found'))
    return violations

#############################################################

 # A few one-liners that are useful for parsing source code
def has_continuation(line):
    return re.match(r'^[^!&\n]*&', line)
def is_declaration(line):
    return re.match(r'^\s*(real|integer|logical|complex|character)\b(?!\s*function)', line, re.IGNORECASE)
def is_blank(line):
    return re.match(r'^\s*$', line)
def is_comment(line):
    return re.match(r'^\s*!.*$', line,)

# Split a line into it's pre-comment and post-comment components. Be careful of string literals!
def split_line_comment(line):
    sq = [q.start() for q in re.finditer(r'\'', line)]
    dq = [q.start() for q in re.finditer(r'\"', line)]
    ex = [x.start() for x in re.finditer(r'!', line)]
    ex_index = None
    for x in ex:
        sq_before = len([q for q in sq if q < x])
        dq_before = len([q for q in dq if q < x])
        if sq_before % 2 == 0 and dq_before % 2 == 0:
            ex_index = x
            break
    if ex_index is None:
        return line, ''
    else:
        return line[0:ex_index], line[ex_index:]

# Splits a line of variable declarations.
def split_variables(vars):
    par_depth = 0 # depth of parentheses we're in
    indices = []
    for i, char in enumerate(vars):
        if char == '(':
            par_depth += 1
        elif char == ')':
            par_depth -= 1
        elif char == ',' and par_depth == 0:
            indices.append(i)
    slices = zip([-1]+indices, indices+[len(vars)])
    split_vars = [vars[i+1:j].strip(' ') for i,j in slices]
    return split_vars
