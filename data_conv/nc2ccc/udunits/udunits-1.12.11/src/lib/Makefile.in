# Makefile for the Unidata units library udunits(3).
#
# $Id: Makefile.in,v 1.9 2008/07/09 22:46:32 steve Exp $

.POSIX:

LIBRARY		= udunits

FLEX		= /usr/bin/flex
cpp_path	= -DUT_DEFAULT_PATH='"$(prefix)/etc/udunits.dat"'
CPPFLAGS	= -I../port/misc -I../port/cfortran $(cpp_path) @CPPFLAGS@
CFLAGS		= @CFLAGS@

OBJS		= utlib.o
LIBOBJS		= utlib.o utparse.o utscan.o udalloc.o

HEADERS		= udunits.h udunits.inc
ETCFILES	= udunits.dat
MANUALS		= udunits.3 udunits.3f
prefix		= ../..
exec_prefix	= $(prefix)/bin

links_udalloc	= udalloc.c udalloc.h
links		= $(links_udalloc)

GARBAGE		= y.output y.output *.log
MANIFEST	= Makefile.in \
		  depend \
		  udunits.3 udunits.3f udunits.3fi testcal2.f \
		  udunits.dat udunits.h udunits.inc.in utlib.c \
		  utparse.y utparse.h \
		  utprivate.h utscan.h utscan.l utscan.c

all:		$(links) library udunits.3f utlib.o

utlib.o:	utlib.c udunits.h

$(links_udalloc):
	ln -s ../port/misc/$@ .

install:	installed_library installed_headers \
	 	installed_etcfiles installed_manuals

clean:		default_clean

distclean:	default_distclean
	rm -f $(links)

udunits.3f:	udunits.3fi
	soelim udunits.3fi >$@

tags:		FORCE utlib.c utparse.c
	ctags -t *.h *.c

# The following emits shift/reduce warning messages that may safely
# be ignored.
#
# A specific yacc(1)-compatible utility is used in order to generate
# a package-specific parser (i.e. one that won't interfere
# with other parsers).
#
utparse.c:	utparse.y
	yacc -b utparse -d -p ut utparse.y
	cat utparse.tab.c > $@
	rm utparse.tab.c
	cat utparse.tab.h > utparse.h
	rm utparse.tab.h

#	case `uname -s` in \
#	OSF1) \
#	    yacc -d -p ut utparse.y; \
#	    cat y.tab.c > $@; \
#	    rm y.tab.c; \
#	    cat y.tab.h > utparse.h; \
#	    rm y.tab.h; \
#	    ;; \
#	*) \
#	    echo 1>&2 "$@ needs to be (re)created using OSF/1's yacc(1)"; \
#	    false; \
#	    ;; \
#	esac

# In the following, a specific utility is used in order to 
# generate a package-specific scanner (i.e. one that won't interfere
# with other scanners).
#
utscan.c:	utscan.l
	@ok=no; \
	if id=`"$(FLEX)" -V 2>/dev/null | \
		sed 's/[^0-9.]*\([0-9.]*\).*/\1/'`; then \
	    maj=`echo $$id | sed 's/\..*//'`; \
	    min=`echo $$id | sed 's/[0-9]*\.\([0-9]*\).*/\1/'`; \
	    if test "$$maj" -gt 2 -o \
		    \( "$$maj" -eq 2 -a "$$min" -ge 4 \); then \
		set -x; \
		$(FLEX) -Put -t utscan.l > $@ && ok=yes; \
		set +x; \
	    fi \
	fi; \
	case $$ok in \
	    no) echo 1>&2 \
		"$@ needs to be (re)created using flex(1) 2.4 or later"; \
		exit 1;; \
	esac

include ../port/master.mk

### Everything after the following line might be overwritten ###
### DO NOT DELETE THIS LINE.  make depend DEPENDS ON IT ###
include depend
