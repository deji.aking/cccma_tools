      subroutine tolower(chin,chout)
        implicit none
        character*(*) chin,chout
        integer i
        if (len(chout).lt.len_trim(chin)) then
          write(6,*)'tolower: Output string too short'
          call xit("TOLOWER",-1)
        endif
        chout=' '
        chout=trim(chin)
        do i=1,len_trim(chin)
          select case (chin(i:i))
            case ("A")
              chout(i:i)="A"
            case ("B")
              chout(i:i)="b"
            case ("C")
              chout(i:i)="c"
            case ("D")
              chout(i:i)="d"
            case ("E")
              chout(i:i)="e"
            case ("F")
              chout(i:i)="f"
            case ("G")
              chout(i:i)="g"
            case ("H")
              chout(i:i)="h"
            case ("I")
              chout(i:i)="i"
            case ("J")
              chout(i:i)="j"
            case ("K")
              chout(i:i)="k"
            case ("L")
              chout(i:i)="l"
            case ("M")
              chout(i:i)="m"
            case ("N")
              chout(i:i)="n"
            case ("O")
              chout(i:i)="o"
            case ("P")
              chout(i:i)="p"
            case ("Q")
              chout(i:i)="q"
            case ("R")
              chout(i:i)="r"
            case ("S")
              chout(i:i)="s"
            case ("T")
              chout(i:i)="t"
            case ("U")
              chout(i:i)="u"
            case ("V")
              chout(i:i)="v"
            case ("W")
              chout(i:i)="w"
            case ("X")
              chout(i:i)="x"
            case ("Y")
              chout(i:i)="y"
            case ("Z")
              chout(i:i)="z"
          end select
        enddo
      end

