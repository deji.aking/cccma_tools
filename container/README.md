# Docker setup for CanESM

Create a docker image with required libs to run CanESM

## Contents

- Based on Ubuntu:18.04
- Adds gcc, gfortran, netcdf, openmpi, git, emacs, vim-tiny, and tmux
- Adds canesm user
- Clones in CanESM source code

## Some commands for building and running

docker build -f Dockerfile.esmf -t cccma/esmf .
docker build -f Dockerfile.canesm -t cccma/canesm-docker .

docker run -it cccma/canesm-docker

docker image prune
docker container prune
